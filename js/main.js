$(function() {
    // Initialize MyQ Responsive Javascript Object
    myqr.init();
});

$(window).load(function () {
    myqr.setEqualBoxHeight();
});

var myqr = myqr || {};

myqr.init = function () {
    myqr.initializeFancySelect();
    myqr.initializeParallax();
    myqr.initializeMobileNav();
    myqr.initializeCardActions();
    myqr.initializeDeviceRules();
    myqr.loadDeviceSlider();
    myqr.setHistoryHeight();
    myqr.setFlipperContainerHeight();
};

myqr.initializeFancySelect = function () {
    // Initialize Fancy Select Plugin on Language Dropdown
    $('.languageSelect-select').fancySelect();
};

myqr.loadDeviceSlider = function () {
    $('.deviceContainer').slick({
        dots: true,
        infinite: true,
        lazyLoad: 'ondemand',
        centerMode: true,
        centerPadding: '0',
        slidesToShow: 3,
        responsive: [
            {
                breakpoint: 768,
                settings: {
                    arrows: true,
                    centerMode: true,
                    centerPadding: '0',
                    slidesToShow: 1
                }
            },
            {
                breakpoint: 480,
                settings: {
                    arrows: false,
                    centerMode: true,
                    centerPadding: '0',
                    slidesToShow: 1
                }
            }
        ]
    });
};

myqr.initializeCardActions = function () {
    $('.js-flipperToggle').click(function(e) {
        myqr.flipperToggle($(this));
        myqr.setFlipperContainerHeight();
    });

    $('.js-managePlaces').click(function(e) {
        var $card = $('.card_managePlaces');

        e.preventDefault();
        myqr.hideAllDeviceCards();
        myqr.flipperToggle($(this));
        $card.fadeIn("fast");
        myqr.adjustFlipperContainerHeight($card);
    });

    $('.js-manageDevice').click(function(e) {
        var $card = $('.card_manageDevices');

        e.preventDefault();
        myqr.hideAllDeviceCards();
        $card.fadeIn("fast");
        myqr.adjustFlipperContainerHeight($card);
    });

    $('.js-addPlace').click(function(e) {
        var $card = $('.card_addPlace');

        e.preventDefault();
        myqr.hideAllDeviceCards();
        $card.fadeIn("fast");
        myqr.adjustFlipperContainerHeight($card);
    });

    $('.js-addPlace2').click(function(e) {
        var $card = $('.card_addPlace2');

        e.preventDefault();
        $(this).children('i').show();
        setTimeout(function() {
            myqr.hideAllDeviceCards();
            $card.fadeIn("fast");
            myqr.adjustFlipperContainerHeight($card);
        }, 3000);
    });

    $('.js-cancelManagePlaces').click(function(e) {
        var $card = $('.card_managePlaces');

        e.preventDefault();
        myqr.flipperToggle($(this));
        myqr.setFlipperContainerHeight();
        setTimeout(function() {
            myqr.hideAllDeviceCards();
            $card.fadeIn("fast");
        }, 1000);
    });

    $('.js-addDevice').click(function(e) {
        var $card = $('.card_addDevice');

        e.preventDefault();
        myqr.hideAllDeviceCards();
        $card.fadeIn("fast");
        myqr.adjustFlipperContainerHeight($card);
    });

    $('.js-addDevice2').click(function(e) {
        var $card = $('.card_addDevice2');

        e.preventDefault();
        myqr.hideAllDeviceCards();
        $card.fadeIn("fast");
        myqr.adjustFlipperContainerHeight($card);
    });

    $('.js-addDevice3').click(function(e) {
        var $card = $('.card_addDevice3');

        e.preventDefault();
        $(this).children('i').show();
        setTimeout(function() {
            $('#registerLoader').hide();
            myqr.hideAllDeviceCards();
            $card.fadeIn("fast");
            myqr.adjustFlipperContainerHeight($card);
            myqr.loadDevice();
        }, 3000);
    });

    $('.js-addRule').click(function(e) {
        var $card = $('.card_addRule');

        e.preventDefault();
        myqr.hideAllRuleCards();
        myqr.flipperToggle($(this));
        $card.fadeIn("fast");
        myqr.adjustFlipperContainerHeight($card);
    });

    $('.js-addRule2').click(function(e) {
        var $card = $('.card_addRule2');

        e.preventDefault();
        myqr.hideAllRuleCards();
        $card.fadeIn("fast");
        myqr.adjustFlipperContainerHeight($card);
    });

    $('.js-cancelManageRules').click(function(e) {
        var $card = $('.card_manageRules');

        e.preventDefault();
        myqr.flipperToggle($(this));
        myqr.setFlipperContainerHeight();
        setTimeout(function() {
            myqr.hideAllRuleCards();
            $card.fadeIn("fast");
        }, 1000);
    });

    $('.toggle').click(function() {
        $(this).toggleClass('is-selected').toggleClass('not-selected');
    });
};

myqr.loadDevice = function() {
    var $card = $('.card_addDevice4');

    setTimeout(function() {
        myqr.hideAllDeviceCards();
        $card.fadeIn("fast");
        myqr.adjustFlipperContainerHeight($card);
    }, 10000);
};

myqr.hideAllDeviceCards = function () {
    $('.flipper-back_devices > .card').hide();
};

myqr.hideAllRuleCards = function () {
    $('.flipper-back_rules > .card').hide();
};

myqr.setFlipperContainerHeight = function () {
    $('.flipper-front > .card').each(function() {
        var height = $(this).outerHeight();
        var container = $(this).closest('.flipperContainer');
        container.css('height', height);
    });
};

myqr.adjustFlipperContainerHeight = function (card) {
    var height = card.outerHeight();
    console.log(height);
    var container = card.closest('.flipperContainer');
    container.css('height', height);
};

myqr.setHistoryHeight = function () {
    var height = $('.rules').outerHeight();
    var container = $('.history');
    container.css('height', height);
};

myqr.setEqualBoxHeight = function () {
    var tallest = 0;

    $('.box').each(function() {
        var thisHeight = $(this).height();
        if (thisHeight > tallest) {
            tallest = thisHeight;
        }
    });

    $('.box').height(tallest + 10);
};

myqr.initializeDeviceRules = function () {
    $('.device-rule label').click(function() {
        var $rule = $(this);
        var $checkbox = $rule.children().find('input[type="checkbox"]');

        if ($rule.hasClass('is-selected')) {
            $checkbox.attr("checked", "checked");
            $rule.removeClass('is-selected').addClass('not-selected');
        } else {
            $checkbox.attr("checked", ""); // For IE
            $checkbox.removeAttr("checked");
            $rule.removeClass('not-selected').addClass('is-selected');
        }
    });
};

myqr.flipperToggle = function (target) {
    var $flipper = target.closest('.flipper');

    if ($flipper.hasClass('flipped')) {
        $flipper.removeClass('flipped');
    } else {
        $flipper.addClass('flipped');
    }
};

myqr.initializeParallax = function () {
    var $win = $(window);
    var $phone = $('.hero-wrap-hand');
    var $body = $('.hero-wrap-hd');
    var parallaxVal = 2.5;
    var opacityVal = 1000;

    $win.scroll(function() {
        var yax = $win.scrollTop();

        $phone.css({
            'transform': 'translateY(' + yax/parallaxVal + 'px)',
            '-webkit-transform': 'translateY(' + yax/parallaxVal + 'px)',
            'opacity': 1 - (yax/opacityVal)
        });

        $body.css({
            'transform': 'translateY(' + yax/parallaxVal + 'px)',
            '-webkit-transform': 'translateY(' + yax/parallaxVal + 'px)',
            'opacity': 1 - (yax/opacityVal)
        });
    });
};

myqr.initializeMobileNav = function () {
    // Toggles Mobile Navigation on the dashboard
    var toggle = $('.js-openMobileNav');
    var icon = toggle.children("i");
    var nav = $('.menuContainer');
    var body = $('.pageContainer');
    var window = $(window);
    var open = function() {
        nav.toggleClass('isOpen');
        body.toggleClass('isOpen');
        icon.toggleClass('fa-reorder');
        icon.toggleClass('fa-times');
    };

    if (window.width() < 767) {
        toggle.click(function(e) {
            open();
        });
    }
};
