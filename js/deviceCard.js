/**
* Contructs a list of device Id keys.
*
* @param dlist
*   List of devices from which to extract Ids.
*
* @return
*   Returns a list containing only Id keys from the given devices.
*/
function getDeviceKeys(dList) {
    var device;

    var idList = [];
    for (var i = 0; i < dList.length; i++) {
        device = dList[i];
        idList.push(device.DeviceId);
    }
    return idList;
}


/**
* Contructs a ManagerModal windows in which to edit device and user information.
*
* @param id
*   HTML Id of the modal
*
* @param opts
*   Object of options for modal, including 'title', 'width', 'height', and 'fixedCenter'.
*
* @return
*   Void
*/
function ManagerCard(id, opts) {
    this.card = $(document.getElementById(id));

    if (opts === null) {
        this.opts = {};
    }
    else {
        this.opts = opts;
    }

    // title
    if (this.opts.title) {
        var title = $(this.modal.find("span#modalTitle")[0]);
        title.html(this.opts.title);
    }

    // size
    if (this.opts.width) {
        this.modal.width(this.opts.width);
    }
    else {
        this.modal.width(835);
    }

    if (this.opts.height) {
        this.modal.height(this.opts.height);
    }
    else {
        this.modal.height(565);
    }

    var header = $(this.modal.find('div.header')[0]);
    header.height(this.HEADER_HEIGHT);

    var body = $(this.modal.find('div.body')[0]);
    body.height(this.modal.height() - this.HEADER_HEIGHT - 2);

    var headerPadding = $(header.find('div.headerPadding')[0]);
    // 1 extra pixel for the border
    headerPadding.height(this.HEADER_HEIGHT - 20 - 1);

    // position
    if (this.opts.fixedcenter === true) {
        var viewportWidth = $(window).width();
        var viewportHeight = $(window).height();
        var scrollHeight = $(window).scrollTop();

        var height = this.modal.height();
        var width = this.modal.width();

        this.modal.css({ "left": (viewportWidth - width) / 2,
            "top": scrollHeight + ((viewportHeight - height) / 2)
        });
    }

    var messageOk = $("#modalMessageOk");
    messageOk.click(this.hideMessage);
}


/**
* Shows the ManagerModal window.
*
* @return
*   Void
*/
ManagerModal.prototype.show = function () {
    this.modal.find("div.modalContent").hide();

    var title = $(this.modal.find("span#modalTitle")[0]);
    title.html(this.opts.title);

    var close = $(".closeWrapper");
    var closeText = $(close.children("span"));
    closeText.html(getString("SaveAndClose"));

    this.modalPageCover.show();
    this.modal.show();
};


/**
* Hides the ManagerModal window.
*
* @return
*   Void
*/
ManagerModal.prototype.hide = function () {
    this.modalPageCover.hide();
    this.modal.hide();
};


/**
* Hides the ManagerModal window.
*
* Unlike hide, this fuction is fired from an event.
*
* @param e
*   Event object that fired the function.
*
* @return
*   Void
*/
ManagerModal.prototype.close = function (e) {
    var self = e.data;
    self.hideMessage();
    self.hide();

    var $close = $(".closeWrapper");
    $close.unbind('click');
};


/**
* Shows a message over the ManagerModal window
*
* @param message
*   Message string to be displayed.
*
* @param buttonLabel
*   String for the label of the button on the message.
*
* @return
*   Void
*/
ManagerModal.prototype.showMessage = function (message, buttonLabel) {
    var messageWrapper = $("span.messageText");
    messageWrapper.html(message);

    if (buttonLabel === null) {
        buttonLabel = getString("Ok");
    }
    $("#modalMessageOk").html(buttonLabel);

    $("#modalMessages").show();
};
ManagerModal.prototype.hideMessage = function () {
    $("#modalMessages").hide();
};
ManagerModal.prototype.HEADER_HEIGHT = 60;
