﻿// device.js
// Mike Sabatini
// 1/31/11
// 10 second timeout for now
var timeoutLength = 10000;
// half a minute for device connection checks
var connectionTimeout = 30000;
var LIGHT_STATE_CHANGE_TIMEOUT = 30000;
var VERSION_CHANGE_TIMEOUT = 300000;
var GATEWAY_FIRMWARE_VERSION_ATTR = "fwver";
var devices = {};
var devicesNotUpdated = [];
var gatewaysNotUpdated = [];
var gateways = {};
var managerModal = null;

// GUI ELEMENT SLIDER CLASS
/**
* Constructs a slider used to control a device.
*
* @param labels
*   2-element array containing strings used to label the states of the slider
*
* @param state
*   Boolean value for the state of the slider. In general, true indicates 'on' or 'open' while false indicates 'closed' or 'off'.
*
* @return
*   Returns a new instance of a slider object.
*/
function Slider(labels, state) {
    this.drag = false;
    this.enabled = true;
    //this.oldX = 0;
    this.error = false;
    this.state = state;

    this.wrapper = $("<div class='slider' />");
    this.wrapper.on("mousedown", { self: this }, this.sliderMouseDown);

    //this.thumb = $("<img/>", { src: assetDirectory + "thumb_button.png", "class": "sliderThumb" });
    // jQuery also allows binding multiple events at once. However, you cannot pass additional data (self)
    //to the handlers without closures which are notorious for creating memory leaks in browsers (IE)
    //this.thumb.bind("mousedown", {self:this}, this.thumbMouseDown);
    //this.thumb.bind("mousemove", {self:this}, this.thumbMouseMove);
    //this.thumb.bind("mouseup", {self:this}, this.thumbMouseUp);
    //this.thumb.bind("mouseout", { self: this }, this.thumbMouseUp);

    //this.labels = labels;
    //this.leftLabel = $("<span class='sliderLabel'>" + labels[0] + "</span>");
    //this.rightLabel = $("<span class='sliderLabel sliderLabelRight'>" + labels[1] + "</span>");

    this.loader = $("<div class='device-loader sliderLoader'><i class='fa fa-refresh fa-spin fa-3x'></i></div>");
    //this.loaderBackground = $("<img class='sliderLoaderBackground' src='" + assetDirectory + "disabled_white_button.png'/>");

    //this.wrapper.append(this.leftLabel);
    //this.wrapper.append(this.rightLabel);
    //this.wrapper.append(this.thumb);

    //var wrapperBackgroundPos = "0px 0px";
    //var thumbPosition = 0;
    //if (state == true) {
    //    wrapperBackgroundPos = "0px 48px";
    //    thumbPosition = 80;
    //}
    //this.wrapper.css("background-position", wrapperBackgroundPos);
    //this.thumb.css("left", thumbPosition + "px");

    return this;
}

/**
* Event handler function for when the slider is clicked or pressed.
*
* After the slider is clicked, the thumb will move to the appropriate side, and the slider will change to the appropriate state.
*
* @param e
*   Event object passed by the listener
*
* @return
*   Void
*/
Slider.prototype.sliderMouseDown = function (e) {
    e.preventDefault();
    var self = e.data.self;
    if (self.enabled == false) {
        if (self.disabledMessage) {
            var alert = new Alert({
                title: getString('Error'),
                message: self.disabledMessage,
                buttons: [{ text: getString("Ok"), handler: function (e) { e.data.alert.hide(); }, object: alert }]
            }).show();
        }
        $(self).trigger("refreshDevice");
        return;
    }

    if (self.state == false) {
        self.slideRight();
    }
    else {
        self.slideLeft();
    }
};
/**
* Event handler function for when the thumb on a slider is clicked or pressed.
*
* When the thumb is clicked or pressed, is it then set to be draggable.
*
* @param e
*   Event object passed by the listener
*
* @return
*   Void
*/
//Slider.prototype.thumbMouseDown = function (e) {
//    e.preventDefault();
//    e.stopImmediatePropagation();

//    var self = e.data.self;
//    if (self.enabled == false) {
//        if (self.disabledMessage) {
//            var alert = new Alert({
//                title: getString('Error'),
//                message: self.disabledMessage,
//                buttons: [{ text: getString("Ok"), handler: function (e) { e.data.alert.hide(); }, object: alert }]
//            }).show();
//        }
//        $(self).trigger("refreshDevice");
//        return;
//    }

//    self.drag = true;
//    self.oldX = e.clientX;
//};
/**
* Event handler function for when the thumb on a slider is dragged.
*
* Updates the position of the thumb on the slider as it is being dragged.
*
* @param e
*   Event object passed by the listener
*
* @return
*   Void
*/
//Slider.prototype.thumbMouseMove = function (e) {
//    e.preventDefault();
//    e.stopImmediatePropagation();
//    var self = e.data.self;
//    if (self.drag) {
//        var rightBound = self.wrapper.width() - self.thumb.width();
//        var newPosition = self.thumb.position().left + e.clientX - self.oldX;
//        if (newPosition < 0) {
//            newPosition = 0;
//        }
//        else if (newPosition > rightBound) {
//            newPosition = rightBound;
//        }

//        self.thumb.css("left", newPosition + "px");
//        self.updateBackground();
//        self.oldX = e.clientX;
//    }
//};
/**
* Event handler function for when a click or press of the thumb is released.
*
* When the thumb is released from being dragged, it animates to the left or right edge of the slider, whichever is closest. If the thumb has changed sides, the STATE_TOGGLED event is fired.
*
* @param e
*   Event object passed by the listener
*
* @return
*   Void
*/
//Slider.prototype.thumbMouseUp = function (e) {
//    e.preventDefault();
//    e.stopImmediatePropagation();
//    var self = e.data.self;

//    if (self.drag) {
//        var wrapperMiddle = self.wrapper.width() / 2;
//        var thumbPosition = self.thumb.position().left;
//        var thumbMiddle = thumbPosition + self.thumb.width() / 2;
//        if (thumbPosition > 0 && thumbMiddle < wrapperMiddle) {
//            self.slideLeft();
//        }
//        else if (thumbMiddle >= wrapperMiddle && thumbPosition + self.thumb.width() < self.wrapper.width()) {
//            self.slideRight();
//        }
//        else if (thumbPosition == 0) {
//            $(self).trigger(STATE_TOGGLED, 0);
//        }
//        else if (thumbPosition == self.wrapper.width() - self.thumb.width()) {
//            $(self).trigger(STATE_TOGGLED, 1);
//        }
//    }

//    self.drag = false;
//};
/**
* Slides the thumb on the slider to the left and fires the STATE_TOGGLED event.
*
* @return
*   Void
*/
Slider.prototype.slideLeft = function (optionalUpdateDevice) {
    //if (typeof optionalUpdateDevice === "undefined") {
    //    optionalUpdateDevice = true;
    //}

    //var self = this;
    //this.thumb.animate({ left: "0px" },
    //    {
    //        duration: 300,
    //        step: function () { self.updateBackground(); },
    //        complete: function () {
    //            if (optionalUpdateDevice) {
    $(this).trigger(STATE_TOGGLED, 0);
        //        }
        //    }
        //});
};
/**
* Slides the thumb on the slider to the right and fires the STATE_TOGGLED event.
*
* @return
*   Void
*/
Slider.prototype.slideRight = function (optionalUpdateDevice) {
    //if (typeof optionalUpdateDevice === "undefined") {
    //    optionalUpdateDevice = true;
    //}
    //var self = this;
    //this.thumb.animate({ left: this.wrapper.width() - this.thumb.width() + "px" },
    //    {
    //        duration: 300,
    //        step: function () { self.updateBackground(); },
    //        complete: function () {
    //            if (optionalUpdateDevice) {
    $(this).trigger(STATE_TOGGLED, 1);
        //        }
        //    }
        //});
};
/**
* Updates the background position of the slider during animations.
*
* Since the thumb is less than half the width of the slider, the background position must be changed relative to the postion of the slider so that the background appears to be a single color.
*
* @return
*   Void
*/
//Slider.prototype.updateBackground = function () {
//    var position = this.thumb.position().left;
//    if (position > this.wrapper.width() / 4) {
//        this.wrapper.css("background-position", "0px -48px");
//    }
//    else if (position < this.wrapper.width() * .75) {
//        this.wrapper.css("background-position", "0px 0px");
//    }
//};
/**
* Overlays the slider with a spinning loading gif for when the device is toggling states.
*
* @return
*   Void
*/
Slider.prototype.showLoading = function () {
    //this.leftLabel.html("");
    //this.rightLabel.html("");
    //this.wrapper.append(this.loaderBackground);
    this.wrapper.append(this.loader);
};
/**
* Removes the loading gif from a slider for when a device is finished toggling states.
*
* @return
*   Void
*/
Slider.prototype.hideLoading = function () {
    //this.leftLabel.html(this.labels[0]);
    //this.rightLabel.html(this.labels[1]);
    //this.loaderBackground.remove();
    this.loader.remove();
};
// DEVICE CLASSES //

// DEVICE/**
/**
* Constructs a new device
*
* @param id
*   ID number used to uniquely identify the device
*
* @param deviceInfo
*   Object containing the typeId and the name of the device.
*
* @return
*   Returns a new instance of a device object.
*/
function Device(id, deviceInfo) {
    this.deviceId = id;
    if (deviceInfo != null) {
        this.deviceType = deviceInfo["typeId"];
        if (deviceInfo[Device.NAME] == null || deviceInfo[Device.NAME][0] == "") {
            this.deviceName = [getString("DefaultDevice"), 0];
        }
        else {
            this.deviceName = deviceInfo[Device.NAME];
        }
    }

    this.setOnline(true);

    this.className = Device.CLASS_NAME;
    this.deviceInfo = deviceInfo;
    this.updateInterval = null;
    this.sliderState = false;

    //var on = getString("On");
    //var off = getString("Off");   

    //this.toggleStateLabels = new Array(on, off);
    this.htmlClass = "device ";
}

/**
* Getter for the name of the device.
*
* @return
*   Returns a string representing the name of the device
*/
Device.prototype.name = function () {
    return this.deviceName[0];
};
/**
* Getter for the ID of the gateway to which the device belongs.
*
* @return
*   Returns the ID number of the gateway to which the device belongs.
*/
Device.prototype.gatewayId = function () {
    if (this.deviceInfo[Device.GATEWAY_ID]) {
        return this.deviceInfo[Device.GATEWAY_ID][0];
    }
    else {
        return -1;
    }
};
/**
* Returns an image for the corresponding state of the device (not used for the general device class).
*
* @return
*   Returns an empty string
*/
Device.prototype.iconFromState = function () {
    return "";
};
/**
* Updates the name of the device
*
* @param info
*   Object containing the new name of the device
*
* @return
*   Void
*/
Device.prototype.updateDevice = function (info) {
    if (info[Device.NAME] == null) {
        info[Device.NAME] = [getString("DefaultDevice"), 0];
    }

    if (this.deviceName != info[Device.NAME][0]) {
        this.deviceName = info[Device.NAME];
        var nameLabel = this.getNameLabel();
        nameLabel.html(this.name());
    }
};
/**
* Converts an absolute time to a single-unit time with a label
*
* Calculates the lapsed time and then simplifies it to it's largest whole-unit measure of time (i.e. seconds, minutes, hours, days).
*
* @param time
*   Time (in milliseconds) when device parameter was last changed.
*
* @return
*   An array of length 2 in which the first element is an integer number of units and the second member is a string label.
*/
Device.prototype.timeToLargestUnit = function (time) {
    var date = new Date(time);
    var now = new Date();

    var difference = new TimeSpan(now - date);

    var days = difference.getDays();
    var months = (days / 30).toFixed(0);

    if (months >= 1) {
        this.registerForUpdates(updater.MONTHS_TIMEOUT);
        if (months < 2) {
            return [months, getString("Month")];
        }
        return [months, getString("Months")];
    }
    if (days >= 1) {
        this.registerForUpdates(updater.DAYS_TIMEOUT);
        if (days < 2) {
            return [days, getString("Day")];
        }
        return [days, getString("Days")];
    }

    var hours = difference.getHours();
    if (hours >= 1) {
        this.registerForUpdates(updater.HOURS_TIMEOUT);
        if (hours < 2) {
            return [hours, getString("Hour")];
        }
        return [hours, getString("Hours")];
    }

    var minutes = difference.getMinutes();
    if (minutes >= 1) {
        this.registerForUpdates(updater.MINUTES_TIMEOUT);
        if (minutes < 2) {
            return [minutes, getString("Minute")];
        }
        return [minutes, getString("Minutes")];
    }

    var seconds = difference.getSeconds();
    if (seconds >= 1) {
        this.registerForUpdates(updater.SECONDS_TIMEOUT);
        if (seconds < 2) {
            return [seconds, getString("Second")];
        }
        return [seconds, getString("Seconds")];
    }

    // 1 second is the smallest we will show them
    this.registerForUpdates(updater.SECONDS_TIMEOUT);
    return [1, getString("Second")];
};
/**
* Adds the device to the updater 
*
* Adding the device to the updater allows the updater to update the time label for the device when appropriate.
*
* @param newInterval
*   Timespan (in milliseconds) for when the time label of the device should be updated.
*
* @return
*   Void
*/
Device.prototype.registerForUpdates = function (newInterval) {
    if (this.updateInterval == null || this.updateInterval != newInterval) {
        updater.registerForUpdate(this, newInterval);
    }
    this.updateInterval = newInterval;
};
/**
* Gets the HTML name label element for the device
*
* @return
*   jQuery object representing the device's name label
*/
Device.prototype.getNameLabel = function () {
    if (this.nameLabel == null) {
        this.nameLabel = $("div#" + this.deviceId + " span.device-name");
    }
    return this.nameLabel;
};
/**
* Sets whether the device is online or offline
*
* @param online
*   Boolean value for whether the device is online (true) and thus controllable, or offline (false).
*
* @return
*   Void
*/
Device.prototype.setOnline = function (online) {
    this.online = online;
    if (this.wrapper) {
        this.slider.enabled = this.online;
        if (this.online == Device.ONLINE) {
            this.wrapper.removeClass(Device.DISABLED_CLASS);
        }
        else {
            this.wrapper.addClass(Device.DISABLED_CLASS);
        }
        this.updateStateLabel();
        this.updateTimeLabel();
    }
};
/*
* Updates the label reflecting the state of a device. Default for a device is Unknown
*
* @return
*   Void
*/
Device.prototype.updateStateLabel = function () {
    var stateLabel = $("div#" + this.deviceId).find("span.deviceStateLabel")[0];
    $(stateLabel).html(getString("Unknown"));
};
Device.prototype.updateTimeLabel = function () { };
/**
* Gets the icon object for the device.
*
* @return
*   jQuery object representing the image icon for the device.
*/
Device.prototype.getIcon = function () {
    if (this.deviceIcon == null) {
        this.deviceIcon = $("div#" + this.deviceId + " img.device-img");
    }
    return this.deviceIcon;
};
/**
* Disables the device's slider from being able to change states.
*
* @return
*   Void
*/
Device.prototype.disableSlider = function () {
    this.slider.enabled = false;
};
/**
* Enables the device's slider.
*
* @return
*   Void
*/
Device.prototype.enableSlider = function () {
    this.slider.enabled = true;
};
/**
* Gets the gateway object to which the device belongs
*
* @return
*   Gateway object that the device is synced to.
*/
Device.prototype.getDeviceGateway = function () {
    if (this.deviceInfo && this.deviceInfo[Device.GATEWAY_ID]) {
        var gatewayId = this.deviceInfo[Device.GATEWAY_ID][0];
        for (var k in gateways) {
            if (gatewayId == gateways[k].deviceId) {
                return gateways[k];
            }
        }
    }
    return null;
};
/**
* Updates the name of the gateway to which the device belongs
*
* @return
*   Void
*/
Device.prototype.updateGatewayName = function () {
    var gateway = this.getDeviceGateway();
    if (gateway != null) {
        this.gatewayName = gateway.deviceName;
        if (this.gatewayName != null) {
            var element = $("div#" + this.deviceId + " div.device-location");
            if (element != null) {
                var newGatewayName = this.gatewayName[0];
                if (gateway.online != Device.ONLINE) {
                    $(element).addClass("offlineGateway");
                    newGatewayName += " " + getString("Offline");
                }
                else {
                    $(element).removeClass("offlineGateway");
                }
                $(element).html(newGatewayName);
            }
        }
    }
};
/**
* Creates an HTML element based on the device
*
* @return
*   HTML string for the device object, including it's slider, name, icon, and time label.
*/
Device.prototype.toHTML = function () {
    this.wrapper = $("<div></div>", { id: this.deviceId, "class": this.htmlClass + "device" });

    if (!this.online) {
        this.wrapper.addClass(Device.DISABLED_CLASS);
    }

    this.slider = new Slider(this.toggleStateLabels, this.sliderState);
    this.slider.enabled = this.online;

    $(this.slider).on(STATE_TOGGLED, { self: this }, this.changeState);
    if (this.className === "light") {
        $(this.slider).bind("refreshDevice", { self: this }, this.checkOnlineTrigger);
    }

    this.wrapper.append("<div class='device-location'></div><div class='device-name'>" + this.deviceName[0] + "</div>");

    var grid = $("<div class='grid'></div>"),
        //iconSliderRow = $("<div ></div>"),
        iconCol = $("<img src='" + this.iconFromState() + "' class='device-img'/>");
        //sliderCol = $("<div class='grid-col grid-col-4of5'></div>"),
        //monitorCol = $("<div class='grid-col grid-col-4of5'></div>");

    //if (this.readableDoorState !== undefined) {
    //    monitorCol = $("<div class='grid-row no-pad'><div class='flatStatus'><div class='flatStatus-icon'><span class='monitorIcon'>Monitor Only</span></div><div class='flatStatus-bd'><span class='flatLabel'>" + this.readableDoorState() + "</span></div></div></div>");
    //}

    //grid.append(iconCol);
    //iconSliderRow.append(iconCol);

    //var self = this;
    //if (this.isMonitorOnly) {
    //    iconSliderRow.append(monitorCol);
    //    monitorCol.click(function () {
    //        if (self.slider.error != true) {
    //            var alert = new Alert({
    //                title: getString('MonitorOnlyErrorTitle'),
    //                message: getString('MonitorOnlyErrorMessage'),
    //                buttons: [{ text: getString("Ok"), handler: function (e) { e.data.alert.hide(); }, object: alert }]
    //            }).show();
    //        }
    //    });
    //}else {
    //    iconSliderRow.append(sliderCol);
    //}
    
    //iconCol.append("<span class='" + this.iconFromState() + "'/>");
    //sliderCol.append(this.slider.wrapper);
    this.slider.wrapper.append(iconCol);
    grid.append(this.slider.wrapper);

    //var img = "<img src='" + this.iconFromState() + "' class='device-img'/>";

    this.wrapper.append(grid);

    return this.wrapper;
};

Device.prototype.getBrand = function ()
{
    // Remove any non-numeric chars from the SN
    var sn = this.deviceName.replace(/[^0-9]/g, '');
    var snBrand = sn.substr(0, 2);
    return (snBrand === "01") ? "chamberlain" : "liftmaster";
}

/**
* Shows an error message alerting that there was an issue communicating with the servers.
*
* @return
*   Void
*/
Device.prototype.showConnectionError = function () {
    var failAlert = new Alert({ title: getString("DeviceError"),
        message: this.name() + " " + getString("RespondFailure"),
        buttons: [{ text: getString("Ok"), handler: function (e) { e.data.alert.hide(); }, object: failAlert}]
    });
    failAlert.show();
    this.slider.hideLoading();
    this.enableSlider();
    //this.resetSlider();
};
Device.CLASS_NAME = "device";
Device.NAME = "desc";
Device.GATEWAY_ID = "gatewayID";
Device.PRESENT = "1";
Device.NOT_PRESENT = "0";
Device.PRESENT = "1";
Device.ONLINE = true;
Device.DISABLED_CLASS = "disabled";
Device.REQUEST_DEVICE_STATUS = "requestdevicestatus";
Device.REQUEST_DEVICE_STATUS_ON = "1";
Device.DELETED_LOCALLY = "deletedlocally";

// Gateway
Gateway.prototype = new Device;
Gateway.prototype.constructor = Gateway;
Gateway.prototype.parent = Device.prototype;

/**
* Constructs a new Gateway
*
* @param id
*   ID number used to uniquely identify the gateway
*
* @param deviceInfo
*   Object containing all given info for the light.
*
* @return
*   Returns a new instance of a gateway object.
*/
function Gateway(id, deviceInfo) {
    if (deviceInfo[Device.NAME] == null) {
        deviceInfo[Device.NAME] = [getString("DefaultGateway"), 0];
    }
    this.hasEnumerated = false;
    Device.call(this, id, deviceInfo);
    this.className = Gateway.CLASS_NAME;
}

/**
* Gets the serial number of the gateway
*
* @return
*   Serial number of gateway as a string.
*/
Gateway.prototype.serialNumber = function () {
    return this.deviceInfo[Gateway.SERIAL_NUMBER][0];
};
/**
* Gets the appropriate icon image based on the state of the gateway
*
* @return
*   Directory path for the image, given as a string
*/
Gateway.prototype.iconFromState = function () {
    return "equipment-icon equipment-icon-gateway";
};
/**
* Gets all devices currently synced to the gateway.
*
* @return
*   Array containing all devices currently synced to the gateway.
*/
Gateway.prototype.getDevices = function () {
    var dList = [];
    for (var id in devices) {
        if (devices[id].gatewayId() == this.deviceId) {
            dList.push(devices[id]);
        }
    }
    return dList;
};
/** 
* Returns if a gateway needs to be enumerated or not
*
* @return
    Boolean - true if enumeate is required, false if not
*/
Gateway.prototype.enumerateRequired = function () {
    if (this.deviceInfo && this.deviceInfo[Gateway.ENUMERATE_REQUIRED]) {
        if (this.deviceInfo[Gateway.ENUMERATE_REQUIRED][0] == Gateway.ENUMERATE_REQUIRED_YES) {
            return true;
        }
    }
    return false;
};
/**
* Updates the name of the gateway.
*
* @param info
*   Object containing the new name for the gateway.
*
* @return
*   Void
*/
Gateway.prototype.updateDevice = function (info) {
    if (info[Device.NAME] == null) {
        info[Device.NAME] = [getString("DefaultGateway"), 0];
    }

    if (info[Gateway.ENUMERATE_REQUIRED]) {
        this.deviceInfo[Gateway.ENUMERATE_REQUIRED] = info[Gateway.ENUMERATE_REQUIRED];
    }

    this.deviceName = info[Device.NAME];

    var devices = this.getDevices();
    var device;
    for (var i = 0; i < devices.length; i++) {
        device = devices[i];
        if (device && device.updateGatewayName) {
            device.updateGatewayName();
        }
    }
};

/**
* Server call to updateFirmware request. Fires user's chosen callback.
*
* @param deviceId
*   the Device ID of the gateway
*
* @param callback
*   callback function that should be fired when finished
*
* @return
*   UserUpdateFirmwareResponse
*/
function updateFirmware(deviceId, callback) {
    PageMethods.updateFirmware(deviceId, callback, handleGenericError);
};

/**
*
*
*
*
*
*/
function pollForUpdatedVersion (deviceId, start, desiredVersion, callback) {
    var self = this;
    var now = new Date();
    var timelapse = now - start;
    
    if (timelapse > VERSION_CHANGE_TIMEOUT) {
        var failAlert = new Alert({
            title: "Hub " + getString("RespondFailure"),
            message: "",
            buttons: [{ text: getString("Cancel"), handler: function () { window.location = "AddDevice4.aspx?deviceId=" + deviceId + "&deviceType=" + TYPE_GATEWAY + "&register=true"; } },
                        { text: getString("TryAgain"), handler: function (e) { retryFirmwareUpdate(e.data.alert, deviceId); }, object: failAlert }]
        });
        failAlert.show();
        return;
    }
    var opt_param = { deviceId: deviceId, start: start, desiredVersion: desiredVersion, callback: callback };
    getDeviceAttribute(deviceId, GATEWAY_FIRMWARE_VERSION_ATTR, pollForUpdatedVersionCallback, null, opt_param);
}

function retryFirmwareUpdate(alert, deviceId) {
    alert.hide();
    // the callback is in /mobile/Management/Add Devices/UpdateFirmware.aspx
    updateFirmware(deviceId, updateFirmware_Callback);
}

/**
*
*
*
*
*
*/
function pollForUpdatedVersionCallback(r, opt_param) {
    
    var deviceId = opt_param.deviceId;
    var start = opt_param.start;
    var desiredVersion = opt_param.desiredVersion;
    var result = jQuery.parseJSON(r);

    var value;
    if (result.result == 0) {
        value = result.payload.AttributeValue;
    }
    else {
        return;
    }
    
    if (value == desiredVersion) {
        if (opt_param.callback)
            opt_param.callback();
    }
    else {
        $.doTimeout(1000, function () {
            pollForUpdatedVersion(deviceId, start, desiredVersion, opt_param.callback); //version isn't updated... poll again in 1 second
        });
    }
};
Gateway.CLASS_NAME = "gateway";
Gateway.SERIAL_NUMBER = "customerName";
Gateway.DESIRED_LEARN_MODE = "desiredlearnmode";
Gateway.ENUMERATE_REQUIRED = "enumeraterequired";
Gateway.LEARN_MODE = "learnmodestate";
Gateway.LEARN_MODE_ON = "1";
Gateway.LEARN_MODE_OFF = "0";
Gateway.ENUMERATE_REQUIRED_YES = "1";
Gateway.ENUMERATE_REQUIRED_NO = "0";
Gateway.ENUMERATE_MODE_LOGIN = 1;
Gateway.ENUMERATE_MODE_REGISTER = 2;

// Light
Light.prototype = new Device;
Light.prototype.constructor = Light;
Light.prototype.parent = Device.prototype;

/**
* Constructs a new Light
*
* @param id
*   ID number used to uniquely identify the light
*
* @param deviceInfo
*   Object containing all given info for the light.
*
* @return
*   Returns a new instance of a light object.
*/
function Light(id, deviceInfo) {
    if (deviceInfo[Device.NAME] == null) {
        deviceInfo[Device.NAME] = [getString("DefaultLight"), 0];
    }
    Device.call(this, id, deviceInfo);

    if (this.deviceInfo[Light.LIGHT_STATE]) {
        var lightState = this.deviceInfo[Light.LIGHT_STATE][0];
        if (lightState == Light.LIGHT_STATE_ON) {
            this.sliderState = true;
        }
    }

    /*if (this.attributes[Light.LIGHT_CONTROL] == Light.LIGHT_STATE_CANNOT_CONTROL) {
        this.setOnline(false);
    }*/

    this.className = Light.CLASS_NAME;
    this.toggleStateLabels = [getString("On"), getString("Off")];
    this.htmlClass += "light ";
}

/**
* Gets the appropriate icon image based on the state of the light
*
* @return
*   Directory path for the image, given as a string
*/
Light.prototype.iconFromState = function (optionalOverrideUnknown) {
    var overrideUnknown = false;
    if (typeof optionalOverrideUnknown === "boolean") {
        overrideUnknown = optionalOverrideUnknown;
    }

    var iconLightOff = "equipment-icon equipment-icon-light-off",
        iconLightOn = "equipment-icon equipment-icon-light-on",
        iconUnknown = "equipment-icon equipment-icon-unknown";

    if (this.deviceInfo[Light.LIGHT_STATE]) {
        switch (this.deviceInfo[Light.LIGHT_STATE][0]) {
            case Light.LIGHT_STATE_ON:
                return iconLightOn;

            case Light.LIGHT_STATE_OFF:
                return iconLightOff;

            default:
                return overrideUnknown ? iconLightOff : iconUnknown;
        }
    }
    else {
        return overrideUnknown ? iconLightOff : iconUnknown;
    }
};
/**
* Gets a string (On/Off/Unknown) based on the boolean state of the light.
*
* @return
*   String "On", "Off", or "Unknown" based on the light's state attribute.
*/
Light.prototype.readableLightState = function () {
    var lightState;

    var gateway = this.getDeviceGateway();
    if (  (gateway != null &&
           gateway.online != Device.ONLINE ) ||
           this.online != Device.ONLINE || 
           this.deviceInfo[Light.LIGHT_STATE] == null ) 
    {
        lightState = getString("Unknown");
    }
    else {
        switch (this.deviceInfo[Light.LIGHT_STATE][0]) {
            case Light.LIGHT_STATE_ON:
                lightState = getString("On");
                break;

            case Light.LIGHT_STATE_OFF:
                lightState = getString("Off");
                break;

            default:
                lightState = getString("Unknown");
                break;
        }
    }
    return lightState;
};
/**
* Sends a request to the server to change the state of the light
*
* If the light is already in the requested state, request is ignored
*
* @param e
*   Event object calling the function
*
* @param state
*   Requested state that the light should go to
*
* @return
*   Void
*/
Light.prototype.changeState = function (e, state) {
    // because this is called by a jquery event bind this is not a GDO but
    //instead an Event Object. So I have to pass myself through the event...not ideal
    var self = e.data.self;
    if (self.deviceInfo && self.deviceInfo[Light.LIGHT_STATE]) {
        if (state == 0 && self.deviceInfo[Light.LIGHT_STATE][0] != Light.LIGHT_STATE_OFF) {
            self.disableSlider();
            self.desiredLightState = Light.DESIRED_LIGHT_STATE_OFF;
            setDeviceAttribute(self.deviceId, Light.DESIRED_LIGHT_STATE, Light.DESIRED_LIGHT_STATE_OFF, self.changeStateCallback, self);
            sendAnalyticsEvent('device-control', 'light_off');
        }
        else if (state == 1 && self.deviceInfo[Light.LIGHT_STATE][0] != Light.LIGHT_STATE_ON) {
            self.disableSlider();
            self.desiredLightState = Light.DESIRED_LIGHT_STATE_ON;
            setDeviceAttribute(self.deviceId, Light.DESIRED_LIGHT_STATE, Light.DESIRED_LIGHT_STATE_ON, self.changeStateCallback, self);
            sendAnalyticsEvent('device-control', 'light_on');
        }
    }
};
/**
* Callback function fired after a server response to a change state request.  Starts polling to see if the state of the light has changed.
*
* @param r
*   JSON server response to the changestate request.
*
* @param lightState
*   State that the light should eventually resolve to.
*
* @return
*   Void
*/
Light.prototype.changeStateCallback = function (result, light) {
    if (result.ReturnCode == 0) {
        var start = new Date();
        light.slider.showLoading();
        light.pollForChanged(start, light.desiredLightState);
    }
    else {
        light.slider.hideLoading();
        light.enableSlider();
        //light.resetSlider();
    }
};
/**
* Checks whether a timeout has occurred and, if not, polls the server for the state of the light.
*
* @param start
*   Time at which polling for a changed state started.  Used as a reference to check for a timeout.
*
* @param desiredFinishState
*   State that the light should resolve to.
*
* @return
*   Void
*/
Light.prototype.pollForChanged = function (start, desiredFinishState) {
    var self = this;
    var now = new Date();
    var timelapse = now - start;
    if (timelapse > LIGHT_STATE_CHANGE_TIMEOUT) {
        var failAlert = new Alert({ title: getString("LightRespondFailure"),
            message: "",
            buttons: [{ text: getString("Ok"), handler: function (e) { e.data.alert.hide(); }, object: failAlert}]
        });
        failAlert.show();
        self.slider.hideLoading();
        self.enableSlider();
        //self.resetSlider();
        return;
    }
    var opt_param = { self: self, start: start, desiredFinishState: desiredFinishState };
    getDeviceAttribute(self.deviceId, Light.LIGHT_STATE, self.pollForChangedCallback, null, opt_param);
};
/**
* Callback function fired after the server responds to a request for the light's state
*
* If the light has reached the desired state, it's slider is enabled, it's loading gif is hidden, and it's timer is reset. If not, it waits one second and the polls the server again for the state of the light.
*
* @param r
*   JSON server response to the light state request.
*
* @param opt_param
*   Object used to pass the light object, start time, and desiredFinishState to the function.
*
* @return
*   Void
*/
Light.prototype.pollForChangedCallback = function (r, opt_param) {
    var self = opt_param.self;
    var start = opt_param.start;
    var desiredFinishState = opt_param.desiredFinishState;
    var result = jQuery.parseJSON(r);
    var value;
    if (result.result == 0) {
        value = result.payload.AttributeValue;
        updateTime = result.payload.UpdatedTime;
    }
    else {
        self.slider.hideLoading();
        self.enableSlider();
        //self.resetSlider;
        return;
    }
    if (value == desiredFinishState) {
        var newInfo = {};
        newInfo[Device.NAME] = [self.deviceName, 0];
        newInfo[Light.LIGHT_STATE] = [value, updateTime];
        self.updateDevice(newInfo);
        self.slider.hideLoading();
        self.enableSlider();
        updater.update();
    }
    else {
        $.doTimeout(1000, function () {
            self.pollForChanged(start, desiredFinishState); //Light not changed... poll again in 1 second
        });
    }
};
/**
* Updates the light's info, including it's name, state, timelabel, slider, and icon
*
* @param info
*   Object containing the light's name, state, and update time
*
* @return
*   Void
*/
Light.prototype.updateDevice = function (info) {
    if (info[Device.NAME] == null) {
        info[Device.NAME] = [getString("DefaultLight"), 0];
    }
    this.parent.updateDevice.call(this, info);

    var update = false;
    if (!this.deviceInfo[Light.LIGHT_STATE]) {
        this.deviceInfo[Light.LIGHT_STATE] = info[Light.LIGHT_STATE];
        update = true;
    }
    else if (this.deviceInfo[Light.LIGHT_STATE][0] != info[Light.LIGHT_STATE][0]) {
        update = true;
    }

    if (update && info[Light.LIGHT_STATE]) {
        var lightstate = info[Light.LIGHT_STATE][0];
        this.deviceInfo[Light.LIGHT_STATE] = info[Light.LIGHT_STATE];

        if (lightstate == Light.LIGHT_STATE_ON) {
            this.sliderState = true;
            this.slider.slideRight();
        }
        else if (lightstate == Light.LIGHT_STATE_OFF) {
            this.sliderState = false;
            this.slider.slideLeft();
        }

        this.updateStateLabel();
        this.updateTimeLabel();

        var newIcon = this.iconFromState();
        var icon = this.getIcon();
        icon.attr("src", newIcon);
    }
};
Light.prototype.updateStateLabel = function () {
    var stateLabel = $("div#" + this.deviceId).find("span.deviceStateLabel")[0];
    $(stateLabel).html(this.readableLightState());
};
/**
* Resets the slider in cases where the light failed to respond to a change of state request
*
* @return
*   Void
*/
//Light.prototype.resetSlider = function () {
//    var lightstate = this.deviceInfo[Light.LIGHT_STATE][0];
//    if (lightstate == Light.LIGHT_STATE_ON) {
//        this.sliderState = true;
//        this.slider.slideRight();
//    }
//    else if (lightstate == Light.LIGHT_STATE_OFF) {
//        this.sliderState = false;
//        this.slider.slideLeft();
//    }
//};
/*
* Call to server to check if Light is currently connected to gateway
*
* @return 
*     Void
*/
Light.prototype.checkOnline = function () {
    //deviceManager.requestDeviceOnline(this, this.deviceOnlineCallback);
};
/*
* Call to server to check if Light is currently connected to gateway
* @e event object calling the function
*
* @return 
*     Void
*/
Light.prototype.checkOnlineTrigger = function (e) {
//    var device = e.data.self;
//    deviceManager.requestDeviceOnline(device, device.deviceOnlineCallback);
};
/*
* Sets the light to online state depending on the return value of the online check
*
* @return
*   Void
*/
Light.prototype.deviceOnlineCallback = function (online, self) {
    self.setOnline(online);
};
/**
* Updates the timelabel for the light based on when it's state has last changed
*
* @return
*   Void
*/
Light.prototype.updateTimeLabel = function () {
    var timeLabel = $("div#" + this.deviceId).find(".text-equipment-time")[0];
    if (this.online == Device.ONLINE && this.deviceInfo[Light.LIGHT_STATE] != null ) {
        var info = this.timeToLargestUnit(parseInt(this.deviceInfo[Light.LIGHT_STATE][1]));
        $(timeLabel).html(" " + getString("TimestampFor") + " " + info.join(" "));
    }
    else {
        $(timeLabel).html("");
    }
};
/**
* Creates an HTML element based on the light
*
* @return
*   HTML string for the light object, including it's slider, name, icon, and time label.
*/
Light.prototype.toHTML = function () {
    var wrapper = this.parent.toHTML.call(this);
    if (this.deviceInfo[Light.LIGHT_STATE]) {
        var lightStateTime = this.timeToLargestUnit(parseInt(this.deviceInfo[Light.LIGHT_STATE][1]));
        var stateLabel = "<span><span class='deviceStateLabel'>" + this.readableLightState() + " </span><span class='text-equipment-time'>" + getString("TimestampFor") + " " + lightStateTime.join(" ") + "</span></span>";
    }
    else {
        var stateLabel = "<span><span class='deviceStateLabel'></span><span class='text-equipment-time'>" + getString("Unknown") + "</span></span>";
    }
    wrapper.append(stateLabel);
    return wrapper;
};
Light.CLASS_NAME = "light";
Light.LIGHT_STATE = "lightstate";
Light.DESIRED_LIGHT_STATE = "desiredlightstate";
Light.LIGHT_STATE_ON = "1";
Light.LIGHT_STATE_OFF = "0";
Light.DESIRED_LIGHT_STATE_OFF = "0";
Light.DESIRED_LIGHT_STATE_ON = "1";



// GDO
GDO.prototype = new Device;
GDO.prototype.constructor = GDO;
GDO.prototype.parent = Device.prototype;

/**
* Constructs a new GDO
*
* @param id
*   ID number used to uniquely identify the GDO
*
* @param deviceInfo
*   Object containing all given info for the GDO.
*
* @return
*   Returns a new instance of a GDO object.
*/
function GDO(id, deviceInfo, isGate, isCDO) {
    // 60 second default door movement timeout
    this.doorActionTimeout = 60000;

    if (deviceInfo != null) {
        if (deviceInfo[Device.NAME] == null) {
            if (isGate) {
                deviceInfo[Device.NAME] = [getString("DefaultGate"), 0];
                // 2 minute action timeout for gates
                this.doorActionTimeout = 120000;
            } else if (isCDO) {
                deviceInfo[Device.NAME] = [getString("DefaultCDO"), 0];
                // 2 minute action timeout for CDO
                this.doorActionTimeout = 120000;
            }
            else {

                deviceInfo[Device.NAME] = [getString("DefaultGDO"), 0];
            }
        }
    }
    Device.call(this, id, deviceInfo);

    if (deviceInfo != null) {
        if (this.deviceInfo[this.DOOR_STATE]) {
            var doorState = this.deviceInfo[this.DOOR_STATE][0];
            if (doorState == this.DOOR_STATE_OPEN || doorState == this.DOOR_STATE_MID_STOP || doorState == this.DOOR_STATE_NOT_CLOSED) {
                this.sliderState = true;
            }
        }
        // else default to false

        if (deviceInfo[GDO.DOOR_STATE_CONTROL] == GDO.DOOR_STATE_CANNOT_CONTROL) {
            this.setOnline(false);
        }

        if (isGate || isCDO) {
            $.doTimeout(60000, function () {
                deviceManager.requestStatusUpdate(id);
                return true;
            });
        }
        
        if (deviceInfo.typeId[0] == TYPE_VGDO) {
            //Fire immediately, then set on timer
            deviceManager.checkVgdoStatus(id);
            $.doTimeout(60000, function () {
                deviceManager.checkVgdoStatus(id);
                return true;
            });
        }
    }
    
    this.className = GDO.CLASS_NAME;
    this.toggleStateLabels = [getString("Open"), getString("Closed")];
    //this.htmlClass += "gdo ";
    this.isGate = isGate;
    this.isCDO = isCDO;
    this.brand = "";
}

/**
* Gets the appropriate icon image based on the state of the GDO
*
* @return
*   Directory path for the image, given as a string
*/
GDO.prototype.iconFromState = function (optionalOverrideUnknown) {
    var overrideUnknown = false;
    var imagePath = "Assets/images/";
    if (typeof optionalOverrideUnknown === "boolean") {
        overrideUnknown = optionalOverrideUnknown;
    }

    var deviceImages = null;
    if (this.isGate) {
        deviceImages = ["equipment-icon equipment-icon-gate-open", "equipment-icon equipment-icon-gate-closed", "equipment-icon equipment-icon-gate-opening"];
    } else if (this.isCDO){
        deviceImages = ["equipment-icon equipment-icon-cdo-open", "equipment-icon equipment-icon-cdo-closed", "equipment-icon equipment-icon-cdo-opening", "equipment-icon equipment-icon-cdo-closing", "equipment-icon equipment-icon-cdo-stopped", "equipment-icon equipment-icon-cdo-unknown"];
    } else {
        deviceImages = [imagePath + "icon_dooropen.png", imagePath + "icon_doorclosed.png", imagePath + "icon_dooropening.png", "equipment-icon equipment-icon-garage-unknown"];
    }

    if (this.deviceInfo[this.UNATTENDED_CLOSE_ALLOWED] && this.deviceInfo[this.UNATTENDED_CLOSE_ALLOWED][0] == 0) {
        this.slider.error = true;
        this.disableSlider();
        this.changeMonitorOnly();
        return deviceImages[3];
    }

    if (this.isMonitorOnly) {
        deviceManager.checkVgdoStatus(this.deviceId);
    }

    this.changeMonitorOnly();

    if (this.deviceInfo[this.DOOR_STATE] && !this.isCDO) {

        switch (this.deviceInfo[this.DOOR_STATE][0]) {
            case this.DOOR_STATE_OPEN:
            case this.DOOR_STATE_NOT_CLOSED:
                return deviceImages[0];

            case this.DOOR_STATE_CLOSED:
                return deviceImages[1];

            case this.DOOR_STATE_RUNNING_UP:
            case this.DOOR_STATE_RUNNING_DOWN:
            case this.DOOR_STATE_MID_STOP:
            case this.DOOR_STATE_MOVING:
                return deviceImages[2];

            default:
                return overrideUnknown ? deviceImages[1] : "equipment-icon equipment-icon-unknown";
        }
    } else if (this.deviceInfo[this.DOOR_STATE] && this.isCDO) {
        switch (this.deviceInfo[this.DOOR_STATE][0]) {
            case this.DOOR_STATE_OPEN:
            case this.DOOR_STATE_NOT_CLOSED:
                return deviceImages[0];

            case this.DOOR_STATE_CLOSED:
                return deviceImages[1];

            case this.DOOR_STATE_RUNNING_UP:
                return deviceImages[2];

            case this.DOOR_STATE_RUNNING_DOWN:
                return deviceImages[3];

            case this.DOOR_STATE_MID_STOP:
                return deviceImages[4];

            case this.DOOR_STATE_MOVING:
                return deviceImages[2];

            default:
                return overrideUnknown ? deviceImages[4] : "equipment-icon equipment-icon-unknown";
        }
    } else {
        return overrideUnknown ? deviceImages[1] : "equipment-icon equipment-icon-unknown";
    }
};
/**
* Gets a string based on the current state of the GDO.
*
* @return
*   String "Open", "Closed", "Stopped", "Opening", "Closing", "Posilockdown", "Autoreverse", or "Unknown" based on the GDO's state attribute.
*/
GDO.prototype.readableDoorState = function () {
    var doorState;
    
    var gateway = this.getDeviceGateway();
    if ((gateway != null &&
           gateway.online != Device.ONLINE) ||
           this.online != Device.ONLINE || 
           this.deviceInfo[this.DOOR_STATE] == null ) {
            doorState = getString("Unknown");
    }
    else {
        switch (this.deviceInfo[this.DOOR_STATE][0]) {
            case "0":
                doorState = getString("Unknown");
                break;

            case this.DOOR_STATE_OPEN:
            case this.DOOR_STATE_NOT_CLOSED:
                doorState = getString("Open");
                break;

            case this.DOOR_STATE_CLOSED:
                doorState = getString("Closed");
                break;

            case this.DOOR_STATE_MID_STOP:
                doorState = getString("Stopped");
                break;

            case "4":
                doorState = getString("Opening");
                break;

            case "5":
                doorState = getString("Closing");
                break;

            case "6":
                doorState = getString("PosilockDown");
                break;

            case "7":
                doorState = getString("AutoReverse");
                break;
            
            case this.DOOR_STATE_MOVING:
                doorState = getString("Moving");
                break;

            default:
                doorState = getString("Unknown");
                break;
        }
    }
    return doorState;
};
/**
* Sends a request to the server to change the state of the GDO
*
* If the GDO is already in the requested state, request is ignored.
*
* @param e
*   Event object calling the function
*
* @param state
*   Requested state that the GDO should go to
*
* @return
*   Void
*/
GDO.prototype.changeState = function (e, state) {
    // because this is called by a jquery event bind this is not a GDO but
    //instead an Event Object. So I have to pass myself through the event...not ideal
    var self = e.data.self;
    
    if (self.deviceInfo && self.deviceInfo[self.DOOR_STATE]) {
        var currentDoorState = self.deviceInfo[self.DOOR_STATE][0];
        if (currentDoorState == 3 && self.deviceType == self.TYPE_CDO && !self.isMonitorOnly) {
            self.disableSlider();
            sendAnalyticsEvent('device-control', 'gdo_close');
            self.targetState = self.DOOR_STATE_CLOSED;
            setDeviceAttribute(self.deviceId, self.DESIRED_DOOR_STATE, self.DESIRED_DOOR_STATE_CLOSED, self.changeStateCallback, self);
        } else if (state == 0 && currentDoorState != self.DOOR_STATE_CLOSED && !self.isMonitorOnly) {
            self.disableSlider();
            sendAnalyticsEvent('device-control', 'gdo_close');
            self.targetState = self.DOOR_STATE_CLOSED;
            setDeviceAttribute(self.deviceId, self.DESIRED_DOOR_STATE, self.DESIRED_DOOR_STATE_CLOSED, self.changeStateCallback, self);
        }
        else if (state == 1 && currentDoorState != self.DOOR_STATE_OPEN && currentDoorState != self.DOOR_STATE_NOT_CLOSED && !self.isMonitorOnly) {
            self.disableSlider();
            sendAnalyticsEvent('device-control', 'gdo_open');
            self.targetState = self.deviceType[0] == TYPE_GDO ? self.DOOR_STATE_OPEN : self.DOOR_STATE_NOT_CLOSED;
            setDeviceAttribute(self.deviceId, self.DESIRED_DOOR_STATE, self.DESIRED_DOOR_STATE_OPEN, self.changeStateCallback, self);
        }
    }
    else {
        self.showConnectionError();
    }
};
/**
* Callback function fired after a server response to a change state request.  Starts polling to see if the state of the GDO has changed.
*
* @param r
*   JSON server response to the changestate request.
*
* @return
*   Void
*/
GDO.prototype.changeStateCallback = function (result, gdo) {
    if (result.ReturnCode == 0) {
        var start = new Date();
        gdo.slider.showLoading();
        gdo.pollForMoving(start);
    }
    else {
        gdo.slider.hideLoading();
        gdo.enableSlider();
        //gdo.resetSlider();
    }
};
/**
* Checks whether a timeout has occurred and, if not, polls the server for the state of the GDO.
*
* @param start
*   Time at which polling for a changed state started.  Used as a reference to check for a timeout.
*
* @return
*   Void
*/
GDO.prototype.pollForMoving = function (start) {
    var self = this;
    var now = new Date();
    var timelapse = now - start;
    if (timelapse > self.doorActionTimeout) {
        self.showConnectionError();
        return;
    }
    var opt_param = { self: self, start: start };
    getDeviceAttribute(self.deviceId, self.DOOR_STATE, self.pollForMovingCallback, null, opt_param);
};
/**
* Callback function fired after the server responds to a request for the GDO's state.
*
* If the GDO has started moving, it starts polling to see if the GDO has  reached a final open or closed state.  If not, it waits one second and then polls again to see if the GDO has started moving.
*
* @param r
*   JSON server response to the GDO state request.
*
* @param opt_param
*   Object used to pass the GDO object and start time to the funciton.
*
* @return
*   Void
*/
GDO.prototype.pollForMovingCallback = function (r, opt_param) {
    var self = opt_param.self;
    var start = opt_param.start;
    var result = jQuery.parseJSON(r);
    var value;
    var updateTime;
    if (result.result == 0) {
        value = result.payload.AttributeValue;
        updateTime = result.payload.UpdatedTime;
    }
    else {
        self.slider.hideLoading();
        self.enableSlider();
        //self.resetSlider();
        return;
    }
    if (value == GDO.prototype.DOOR_STATE_RUNNING_UP) {
        var newInfo = [];
        newInfo[Device.NAME] = [self.deviceName, 0];
        newInfo[self.DOOR_STATE] = [value, updateTime];
        self.updateDevice(newInfo);
        self.pollForFinished(new Date(), self.DOOR_STATE_OPEN);  //Door is moving up... now start polling to see if it is open
    }
    else if (value == GDO.prototype.DOOR_STATE_RUNNING_DOWN) {
        var newInfo = [];
        newInfo[Device.NAME] = [self.deviceName, 0];
        newInfo[self.DOOR_STATE] = [value, updateTime];
        self.updateDevice(newInfo);
        self.pollForFinished(new Date(), self.DOOR_STATE_CLOSED);  //Door is moving down... now start polling to see if it is closed
    }
    else if (value == GDO.prototype.DOOR_STATE_MOVING) {
        var newInfo = [];
        newInfo[Device.NAME] = [self.deviceName, 0];
        newInfo[self.DOOR_STATE] = [value, updateTime];
        self.updateDevice(newInfo);
        if (self.targetState === self.DOOR_STATE_CLOSED) {
            self.pollForFinished(new Date(), self.DOOR_STATE_CLOSED);
        }
        else if (self.targetState === self.DOOR_STATE_NOT_CLOSED) {
            self.pollForFinished(new Date(), self.DOOR_STATE_NOT_CLOSED);
        }
    }
    else if (value == self.targetState) {
        self.pollForFinished(new Date(), self.targetState);
    }
    else {
        $.doTimeout(1000, function () {
            self.pollForMoving(start); //Door is still not moving... poll again in 1 second
        });
    }
};
/**
* Checks whether a timeout has occurred and, if not, polls the server to see if the GDO has reached it's final open or closed state.
*
* @param start
*   Time at which polling for a finshed state started.  Used as a reference to check for a timeout.
*
* @param finish_state
*   State that the GDO should resolve to.
*
* @return
*   Void
*/
GDO.prototype.pollForFinished = function (start, finish_state) {
    var self = this;
    self.updateTimeLabel();
    var now = new Date();
    var timelapse = now - start;
    if (timelapse > 120000) {
        var failAlert = new Alert({ title: getString("NoResponseTitle"),
            message: self.deviceName[0] + " " + getString("NoDeviceResponseMessage"),
            buttons: [{ text: getString("Ok"), handler: function (e) { e.data.alert.hide(); }, object: failAlert}]
        });
        failAlert.show();
        self.slider.hideLoading();
        self.enableSlider();
        //self.resetSlider();
        getDeviceList();
        return;
    }
    var opt_param = { self: self, start: start, finish_state: finish_state };
    getDeviceAttribute(self.deviceId, self.DOOR_STATE, self.pollForFinishedCallback, null, opt_param);
};
/**
* Callback function fired after the server responds to a request for the GDO's state.
*
* If the GDO has reached the desired state, it's slider is enabled, it's loading gif is hidden, and it's timer is reset. If not, it waits one second and the polls the server again for the state of the GDO.
*
* @param r
*   JSON server response to the GDO state request.
*
* @param opt_param
*   Object used to pass the GDO object, start time, and desiredFinishState to the function.
*
* @return
*   Void
*/
GDO.prototype.pollForFinishedCallback = function (r, opt_param) {
    var self = opt_param.self;
    var start = opt_param.start;
    var finish_state = opt_param.finish_state;
    var result = jQuery.parseJSON(r);
    var value;
    var updateTime;
    if (result.result == 0) {
        value = result.payload.AttributeValue;
        updateTime = result.payload.UpdatedTime;
    } else {
        self.slider.hideLoading();
        self.enableSlider();
        getDeviceList();
    }

    if (value == self.DOOR_STATE_OPEN || value == self.DOOR_STATE_CLOSED || value == self.DOOR_STATE_NOT_CLOSED) {
        var newInfo = [];
        newInfo[Device.NAME] = [self.deviceName, 0];
        newInfo[self.DOOR_STATE] = [value, updateTime];
        self.updateDevice(newInfo);
        self.slider.hideLoading();
        self.slider.state = self.slider.state ? false : true;
        self.enableSlider();
        if (value != finish_state) { //Check to see if the finished state is the intended one. If not, throw alert and update devices
            var failAlert = new Alert({
                title: getString("NoResponseTitle"),
                message: self.deviceName[0] + " " + getString("NoDeviceResponseMessage"),
                buttons: [{ text: getString("Ok"), handler: function (e) { e.data.alert.hide(); }, object: failAlert}]
            });
            failAlert.show();
            getDeviceList();
        }
        self.targetState = undefined;
        updater.update(); //Force updater into update loop, since timeInterval is now seconds

        // door stopped mid state should result in a timeout.  JIRA MyQ 1483
        //} else if (value == self.DOOR_STATE_MID_STOP) {
        //    self.slider.hideLoading();
        //    self.enableSlider();
        //    self.resetSlider();

    } else {
        $.doTimeout(1000, function() {
            self.pollForFinished(start, finish_state); //Door is still not open/closed... check again in 1 second
        });
    }
};
/**
* Resets the slider in cases where the GDO failed to respond to a change of state request
*
* @return
*   Void
*/
//GDO.prototype.resetSlider = function () {
//    if (this.deviceInfo && this.deviceInfo[this.DOOR_STATE]) {
//        var doorstate = this.deviceInfo[this.DOOR_STATE][0];
//        if (doorstate == this.DOOR_STATE_OPEN) {
//            this.sliderState = true;
//            this.slider.slideRight();
//        } else if (doorstate == this.DOOR_STATE_CLOSED) {
//            this.sliderState = false;
//            this.slider.slideLeft();
//        } else if (doorstate == this.DOOR_STATE_MID_STOP ||
//            doorstate == this.DOOR_STATE_RUNNING_DOWN ||
//            doorstate == this.DOOR_STATE_RUNNING_UP) {

//            // Reset to Open
//            this.sliderState = true;
//            this.slider.slideRight(false);
//        }
//    }
//};
/**
* Updates the GDO's info, including it's name, state, timelabel, slider, and icon
*
* @param info
*   Object containing the GDO's name, state, update time, and online control.
*
* @return
*   Void
*/
GDO.prototype.updateDevice = function (info) {
    if (info[Device.NAME] == null) {
        if (this.isGate) {
            info[Device.NAME] = [getString("DefaultGate"), 0];
        } else {
            info[Device.NAME] = [getString("DefaultGDO"), 0];
        }
    }
    this.parent.updateDevice.call(this, info);

    var update = false;
    if (!this.deviceInfo[this.DOOR_STATE]) {
        this.deviceInfo[this.DOOR_STATE] = info[this.DOOR_STATE];
        update = true;
    }
    else if (this.deviceInfo[this.DOOR_STATE][0] != info[this.DOOR_STATE][0]) {
        update = true;
    }

    if (info[this.UNATTENDED_CLOSE_ALLOWED] && this.deviceInfo[this.UNATTENDED_CLOSE_ALLOWED] && this.deviceInfo[this.UNATTENDED_CLOSE_ALLOWED][0] != info[this.UNATTENDED_CLOSE_ALLOWED][0]) {
        if (info[this.UNATTENDED_CLOSE_ALLOWED][0] == 0) {
            this.slider.error = true;
            this.disableSlider();
        } else {
            this.slider.error = false;
            this.enableSlider();
        }
        this.deviceInfo[this.UNATTENDED_CLOSE_ALLOWED][0] = info[this.UNATTENDED_CLOSE_ALLOWED][0];
        this.changeMonitorOnly();
        update = true;
    }
    
    if (update && info[this.DOOR_STATE]) {
        var doorstate = info[this.DOOR_STATE][0];
        this.deviceInfo[this.DOOR_STATE] = info[this.DOOR_STATE];

        if (doorstate == this.DOOR_STATE_OPEN ||
            doorstate == this.DOOR_STATE_NOT_CLOSED) {
            this.sliderState = true;
            this.slider.slideRight();
        }
        else if (doorstate == this.DOOR_STATE_CLOSED) {
            this.sliderState = false;
            this.slider.slideLeft();
        }

        else if (doorstate == this.DOOR_STATE_RUNNING_UP) {
        }

        else if (doorstate == this.DOOR_STATE_RUNNING_DOWN) {
        }
        
        else if (doorstate == this.DOOR_STATE_MOVING) {
        }

        this.updateStateLabel();
        this.updateTimeLabel();
        if (this.isMonitorOnly) {
            this.updateMonitorLabel();
        }
        
        var newIcon = this.iconFromState();
        var icon = this.getIcon();
        icon.attr("src", newIcon);
    }

    if (info[GDO.DOOR_STATE_CONTROL] == GDO.DOOR_STATE_CANNOT_CONTROL) {
        this.setOnline(false);
    }
};

GDO.prototype.changeMonitorOnly = function () {
    var self = this;
    var gridRow = $('div#' + this.deviceId).find('div.grid');
    gridRow.on('click', function () {
        if (self.slider.error == true) {
            var alert = new Alert({
                title: getString('Error'),
                message: getString('MonitorOnlyError'),
                buttons: [{ text: getString("Ok"), handler: function (e) { e.data.alert.hide(); }, object: alert }]
            }).show();
        }
    });
};

/**
* Updates a GDO's label if it's in monitor-only mode
*
* @return
*   Void
*/
GDO.prototype.updateMonitorLabel = function() {
    var monitorLabel = $('div#' + this.deviceId).find('div.flatStatus .flatLabel');
    if (monitorLabel) {
        $(monitorLabel).html(this.readableDoorState());
    }
};
/*
* Updates the html label with the current state
*
* @return
*   Void
*/
GDO.prototype.updateStateLabel = function () {
    var stateLabel = $("div#" + this.deviceId).find("span.deviceStateLabel")[0];
    if (stateLabel) {
        $(stateLabel).html(this.readableDoorState());
    }
};
/**
* Updates the timelabel for the GDO based on when it's state has last changed
*
* @return
*   Void
*/
GDO.prototype.updateTimeLabel = function () {
    var timeLabel = $("div#" + this.deviceId).find(".text-equipment-time")[0];
    if (this.online == Device.ONLINE && this.deviceInfo[this.DOOR_STATE] != null) {
        var info = this.timeToLargestUnit(parseInt(this.deviceInfo[this.DOOR_STATE][1]));
        $(timeLabel).html(" " + getString("TimestampFor") + " " + info.join(" "));
    }
    else {
        $(timeLabel).html("");
    }
};
/**
* After server puts GDO online, GDO is updated to reflect the change
*
* @return
*   Void
*/
GDO.prototype.deviceOnlineCallback = function (online, self) {
    self.setOnline(online);
};

/**
* Creates an HTML element based on the GDO
*
* @return
*   HTML string for the GDO object, including it's slider, name, icon, and time label.
*/
GDO.prototype.toHTML = function (scope) {
    var self = scope || this;
    var wrapper = this.parent.toHTML.call(self),
        stateLabel;
    if (self.deviceInfo[self.DOOR_STATE]) {
        var doorStateTime = self.timeToLargestUnit(parseInt(self.deviceInfo[self.DOOR_STATE][1]));
        stateLabel = "<div><span class='deviceStateLabel'>" + self.readableDoorState().toString() + " </span><span class='text-equipment-time'>" + getString("TimestampFor") + " " + doorStateTime.join(" ") + "</span></div>";
        //wrapper.prepend("<div class='device-logo logo-gdo'></div>");
    }
    else {
        stateLabel = "<div><span class='deviceStateLabel'></span><span class='text-equipment-time'>" + getString("Unknown") + "</span></div>";
    }
    wrapper.append(stateLabel);
    return wrapper;
};
GDO.prototype.DOOR_STATE = "doorstate";
GDO.prototype.DESIRED_DOOR_STATE = "desireddoorstate";
GDO.prototype.UNATTENDED_CLOSE_ALLOWED = "isunattendedcloseallowed";
GDO.prototype.DOOR_STATE_OPEN = "1";
GDO.prototype.DOOR_STATE_CLOSED = "2";
GDO.prototype.DOOR_STATE_MID_STOP = "3";
GDO.prototype.DOOR_STATE_RUNNING_UP = "4";
GDO.prototype.DOOR_STATE_RUNNING_DOWN = "5";
GDO.prototype.DOOR_STATE_MOVING = "8";
GDO.prototype.DOOR_STATE_NOT_CLOSED = "9";
GDO.prototype.DESIRED_DOOR_STATE_OPEN = "1";
GDO.prototype.DESIRED_DOOR_STATE_CLOSED = "0";
GDO.CLASS_NAME = "gdo";
GDO.DOOR_STATE_CONTROL = "doorstatecontrol";
GDO.DOOR_STATE_CAN_CONTROL = "1";
GDO.DOOR_STATE_CANNOT_CONTROL = "0";

// END DEVICE CLASSES //

// Device Updater
/**
* Constructs an updater
*
* The updater object handles updating all the device's timelabels
*
* @return
*   Returns a new instance of an updater object.
*/
function Updater() {
    this.updateList = {};
    this.updateList[this.SECONDS_TIMEOUT] = {};
    this.updateList[this.MINUTES_TIMEOUT] = {};
    this.updateList[this.HOURS_TIMEOUT] = {};
    this.updateList[this.DAYS_TIMEOUT] = {};
    this.updateList[this.MONTHS_TIMEOUT] = {};

    this.interval = this.SECONDS_TIMEOUT;
    $.doTimeout(this.interval, this.update);
}

/**
* Registers a device with the updater
*
* The updater maintains a list of devices and their update intervals, and updates devices on the shortest common interval.
*
* @param device
*   Device object that is being registered
*
* @param interval
*   Time interval (seconds,minutes, hours) for which the device must be updated
*
* @return
*   Void
*/
Updater.prototype.registerForUpdate = function (device, interval) {
    var self = updater;
    if (device.updateTimeLabel != null) {
        var lowestCommonInterval = null;
        self.updateList[interval][device.deviceId] = device;
        for (var key in self.updateList) {
            if (key != interval) {
                if (self.updateList[key][device.deviceId] != null) {
                    self.updateList[key][device.deviceId] = null;
                    delete self.updateList[key][device.deviceId];
                }
            }

            if (lowestCommonInterval == null &&
            Object.size(self.updateList[key]) > 0) {
                lowestCommonInterval = key;
            }
        }
        self.interval = lowestCommonInterval;

        // our interval may have changed so fire on the new one to reset the timer
        $.doTimeout('updater', self.interval, function () {
            self.update();
        });
    }
};
/**
* Updates timelabels of all devices and sets a timeout to update again.
*
* @return
*   Void
*/
Updater.prototype.update = function () {

    var self = updater;
    for (var k in updater.updateList) {
        for (var i in updater.updateList[k]) {
            updater.updateList[k][i].updateTimeLabel();
        }
    }

    $.doTimeout('updater', self.interval, function () {
        self.update();
    });
};
Updater.prototype.SECONDS_TIMEOUT = 1000;
Updater.prototype.MINUTES_TIMEOUT = 1000 * 60;
Updater.prototype.HOURS_TIMEOUT = 1000 * 60 * 60;
Updater.prototype.DAYS_TIMEOUT = 1000 * 60 * 60 * 24;
Updater.prototype.MONTHS_TIMEOUT = 1000 * 60 * 60 * 24 * 30;


// DEVICE PAGE FUNCTIONS

// PC WEB ONLY
/**
* Displays the Manage Devices window
*
* @return
*   Void
*/
function showManageDevices() {
    manageDevices = new ManageDevices('manageModal', { "fixedcenter": true, "title": getString("ManagePlacesModalLabel") });
    manageDevices.show();
    $('#addGatewayButton').removeAttr('disabled');
    manageDevices.setGateways(getSortedGatewayList());
}

/**
* Displays page overlays when modals are shown
*
* @return
*   Void
*/
function showOverlay() {
    $("#modalPageCover").show();
    $("#helpOverlay").show();
    $("#helpOverlayClickHandler").show();
}

/**
* Hides page overlays used with modals
*
* @return
*   Void
*/
function hideOverlay(e) {
    if (!e) {
        var e = window.event;
    }
    e.cancelBubble = true;
    if (e.stopPropagation) {
        e.stopPropagation();
    }

    $("#modalPageCover").hide();
    $("#helpOverlay").hide();
    $("#helpOverlayClickHandler").hide();
}

/**
* Sends a request to the server to set a specific attribute for a device
*
* @param id
*   Id number of the target device.
*
* @param attr
*   Device's attribute that is to be set.
*
* @param value
*   Value that you wish to set the attribute to.
*
* @param callback
*   Callback function to be fired after a server response.
*
* @param callback_obj
*   Object to be passed into the callback function as a parameter.
*
* @return
*   Void
*/
function setDeviceAttribute(id, attr, value, callback, callback_obj) {
    var obj = { callback: callback, callback_obj: callback_obj };
    PageMethods.setDeviceAttribute(id, attr, value, setDeviceAttributeCallback, handleGenericError, obj);
}

/**
* Callback function that handle errors after a setDeviceAttribute request and then fires the callback function passed in setDeviceAttribute.
*
* @param r
*   JSON response from server to the setDeviceAttribute request
*
* @param obj
*   Object containing the callback function and callback object passed to the server in setDeviceAttribute
*
* @return
*   Void
*/
function setDeviceAttributeCallback(result, obj) {
    if (result.ReturnCode == 0) {
        obj.callback(result, obj.callback_obj);
    }
    else {
        var failAlert = new Alert({ title: getString("DeviceError"),
            message: canPassErrorToUser(result.ReturnCode) ? result.ErrorMessage : "",
            buttons: [{ text: getString("Ok"), handler: function (e) { e.data.alert.hide(); }, object: failAlert}]
        });
        failAlert.show();
        obj.callback(result, obj.callback_obj);
    }
}

/**
* Sends a request to the server to get a specific attribute for a device
*
* @param id
*   Id number of the target device.
*
* @param attr
*   Attribute to be retrieved.
*
* @param success_callback
*   Callback function to be fired after a successful server response.
*
* @param failure_callback
*   Callback function to be fired after a failed server response.
*
* @param callback_obj
*   Object to be passed into the callback function as a parameter.
*
* @return
*   Void
*/
function getDeviceAttribute(id, attr, success_callback, failure_callback, callback_obj) {
    var obj = { callback: success_callback, callback_obj: callback_obj };
    if (!failure_callback) {
        failure_callback = handleGenericError;
    }
    PageMethods.getDeviceAttribute(id, attr, getDeviceAttributeCallback, failure_callback, obj);
}

/**
* Callback function that handle errors after a getDeviceAttribute request and then fires the callback function passed in getDeviceAttribute.
*
* @param r
*   JSON response from server to the getDeviceAttribute request
*
* @param obj
*   Object containing the callback function and callback object passed to the server in getDeviceAttribute
*
* @return
*   Void
*/
function getDeviceAttributeCallback(r, obj) {
    var result = jQuery.parseJSON(r);
    if (result.result == 0) {
        obj.callback(r, obj.callback_obj);
    }
    else {
        var failAlert = new Alert({ title: getString("DeviceError"),
            message: "",
            buttons: [{ text: getString("Ok"), handler: function (e) { e.data.alert.hide(); }, object: failAlert}]
        });
        failAlert.show();
        obj.callback(r, obj.callback_obj);
    }
}

/*
* Sort function for devices. Takes two device objects and compares the dates they were added
*
* @param a
*    Device object to compare
*
* @param b
*    Second device object to compare
*
* @return
*   0 if data added is the same, -1 if device a's date is before device b's date, and 1 if the opposite
*/
function deviceDateSort(a, b) {
    if (a.deviceInfo && a.deviceInfo[Device.DATE_ADDED]
    && b.deviceInfo && b.deviceInfo[Device.DATE_ADDED]) {

        var aDate = a.deviceInfo[Device.DATE_ADDED][0];
        var bDate = b.deviceInfo[Device.DATE_ADDED][0];

        if (aDate < bDate) {
            return 1;
        }
        else if (bDate < aDate) {
            return -1;
        }
    }
    return 0;
}

/*
* Function to retrieve a sorted list of devices sorted by their added date
*
* @return
*    Array of devices in sorted order
*/
function getSortedDeviceList() {
    var list = [];
    for (var k in devices) {
        list.push(devices[k]);
    }

    return list.sort(deviceDateSort);
}

/*
* Function to retrieve a sorted list of gateways sorted by their added date
*
* @return
*    Array of gateways in sorted order
*/
function getSortedGatewayList() {
    var list = [];
    for (var k in gateways) {
        list.push(gateways[k]);
    }

    return list.sort(deviceDateSort);
}

var enumAlert;
/*
* Callback for gateway enumeration. Called only once per enumeration "batch". 
* Alerts user if enumeration failed
*
* @param success
*     Boolean - the result of the enumerations for all needed gateways at a given time
*
* @return
*   Void
*/
var gatewayEnumerationComplete = function (success) {
    if (enumAlert) {
        enumAlert.hide();
    }
    if (success == false) {
        var failedAlert = new Alert({ title: getString("EnumerateFailedTitle"),
            message: getString("EnumerateFailedMessage"),
            buttons: [{ text: getString("Ok"), handler: function (e) { e.data.alert.hide(); } }]
        });
    }
};
var manageDevices;

/**
* Removes a device from the device list and elimates visual elements for the device.
*
* @param id
*    Id of device to be removed
*
* @return
*   Void
*/
function removeDevice(id) {
    delete devices[id];

    $("#" + id).remove();

    var htmlDevices = $("div.device");
    //if (htmlDevices.length > 0) {
    //    //$(htmlDevices[htmlDevices.length - 1]).addClass("deviceLastChild");
    //}
    //else
    if (htmlDevices.length == 0) {
        var noDeviceMessage = isOnMobilePage() ? getString("NoDevicesMessageMobile") : getString("NoDevicesMessage");
        $("#noDeviceMessage").html(noDeviceMessage);
        $("#noDeviceMessage").show();
    }

    // this line is only for the pc web...
    //remove the manager modal entry for the device if it exists
    if (manageDevices != null) {
        manageDevices.deviceRemoved(id);
    }
    getDeviceList();
}
/**
* Calls getDeviceList in order to update all devices after a device has been deleted locally;
*
* @param r
*    JSON server response to removeDeviceForDeletedLocally
*
* @param obj
*   object passed from the removeDeviceForDeletedLocally server response
*
* @return
*   Void
*/
var removeDeviceForDeletedLocallyCallback = function (r, obj) {
    getDeviceList();
};
/**
* Polls the server to a complete list of devices that are registered with the account.
*
* @return
*   Void
*/
var getDeviceList = function (optionalCallback) {
    // copy all keys to this list
    devicesNotUpdated = [];
    for (var id in devices) {
        devicesNotUpdated.push(id);
    }

    gatewaysNotUpdated = [];
    for (var id in gateways) {
        gatewaysNotUpdated.push(id);
    }

    var callback = getDeviceListCallback;
    if (typeof optionalCallback === "function") {
        callback = function (r) {
            getDeviceListCallback(r);
            optionalCallback(r);
        };
    }

    PageMethods.getDeviceList(callback, handleGenericError);
};
var previousDeviceResponse = "";
var updater;

/**
* Handles the server response to getDeviceList.  
*
* Creates new device objects for any devices that were not previously present, and updates information for all devices.
*
* @param r
*    JSON server response to getDeviceList
*
* @return
*   Void
*/
var getDeviceListCallback = function (r) {

    $('#js-places-loading-indicator').hide();

    if (previousDeviceResponse != r) {
        var result = jQuery.parseJSON(r);

        if (result.result === -1 && result.payload === -1) {
            window.location.href = "/login.aspx";
        }
        else if (result.result === -1 && result.payload === -2) {
            $("#noDeviceMessage").html(getString("DeviceFetchError"));
            $("#noDeviceMessage").show();
            $("#deviceList").children(".device").remove();
            devices = {};
        }
        else {
            var previousResult = previousDeviceResponse != '' ? jQuery.parseJSON(previousDeviceResponse) : null;
            var deviceList = result["payload"];
            var deviceListColumn = $("#deviceList");

            var deleteList = $.extend(true, [], devicesNotUpdated);

            if (previousResult != null &&
            (previousResult.result == -1 || deviceList.length > 0)) {
                $("#noDeviceMessage").hide();
            }

            var deviceInfo, devType, device, gateway, oldDeviceInfo;

            for (var id in deviceList) {
                device = null;
                gateway = null;
                deviceInfo = deviceList[id],
                devType = deviceInfo.typeId[0];
                
                if (deviceInfo[Device.DELETED_LOCALLY] != null) {
                    deviceManager.removeDeviceFromUser(id, removeDeviceForDeletedLocallyCallback, null);
                }

                // device did not previously exist. create one and append to dom
                if (devices[id] == null) {
                    switch (devType) {
                        case TYPE_GDO:
                            device = new GDO(id, deviceInfo);
                            break;
                            
                        case TYPE_VGDO:
                            device = new GDO(id, deviceInfo);
                            if (deviceInfo.myqmonitormode[0] == "1") {
                                device.isMonitorOnly = true;
                            }
                            break;

                        case TYPE_LIGHT:
                            device = new Light(id, deviceInfo);
                            break;

                        case TYPE_GATE:
                            device = new GDO(id, deviceInfo, true);
                            device.isGate = true;
                            break;
                        case TYPE_CDO:
                            device = new GDO(id, deviceInfo, false, true);
                            break;
                        default:
                            device = null;
                            break;
                    }

                    if (device != null) {
                        deviceManager.requestStatusUpdate(device.deviceId);

                        // get rid of previous last child
                        //$("#deviceList div.device:last").removeClass("deviceLastChild");
                        $("#noDeviceMessage").hide();

                        devices[id] = device;

                        var sortedList = getSortedDeviceList();

                        var index;
                        // for some reason IE8 returns this sort as a weird object that behaves 
                        //like an array that does not have indexOf. So I do this instead.
                        for (var j = 0; j < sortedList.length; j++) {
                            if (sortedList[j] == device) {
                                index = j;
                                break;
                            }
                        }

                        //if (index == 0) {
                        deviceListColumn.slickAdd(device.toHTML());
                        $('#js-places-loading-indicator').hide();
                        //}
                        //else {
                        //    var previousNode = deviceListColumn.find("div#" + sortedList[index - 1].deviceId);
                        //    $(previousNode).after(device.toHTML());
                        //}

                        // add new one
                        //$("#deviceList div.device:last").addClass("deviceLastChild");
                    }
                    else {
                        // invalid device
                    }
                }
                else {
                    var index = $.inArray(id, deleteList);
                    if (index != -1) {
                        deleteList.splice(index, 1);
                    }

                    // this device may have changed, update it
                    devices[id].updateDevice(deviceInfo);
                    devices[id].deviceInfo = deviceInfo;
                }

                // it could be a gateway
                if (deviceInfo && deviceInfo.typeId[0] == TYPE_GATEWAY) {
                    // new gateway
                    if (gateways[id] == null) {
                        gateway = new Gateway(id, deviceInfo);
                        gateways[id] = gateway;
                    }
                    // updated gateway
                    else {
                        var index = $.inArray(id, gatewaysNotUpdated);
                        if (index != -1) {
                            gatewaysNotUpdated.splice(index, 1);
                        }

                        gateway = gateways[id];
                        gateway.updateDevice(deviceList[id]);
                    }
                }
            }
            if (Object.size(devices) == 0) {
                var noDeviceMessage = isOnMobilePage() ? getString("NoDevicesMessageMobile") : getString("NoDevicesMessage");
                $("#noDeviceMessage").html(noDeviceMessage);
                $("#noDeviceMessage").show();
            }


            for (var i = 0; i < deleteList.length; i++) {
                var id = deleteList[i];
                removeDevice(id);
            }
        }

        for (var k in devices) {
            devices[k].updateGatewayName();
        }

        previousDeviceResponse = r;
    }
    // else no change in any data
    $.doTimeout('getDeviceList', timeoutLength, getDeviceList);
};
