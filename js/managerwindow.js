﻿/**
* Contructs a list of device Id keys.
*
* @param dlist
*   List of devices from which to extract Ids.
*
* @return
*   Returns a list containing only Id keys from the given devices.
*/
function getDeviceKeys(dList) {
    var device;

    var idList = [];
    for (var i = 0; i < dList.length; i++) {
        device = dList[i];
        idList.push(device["DeviceId"]);
    }
    return idList;
}


/**
* Contructs a ManagerModal windows in which to edit device and user information.
*
* @param id
*   HTML Id of the modal
*
* @param opts
*   Object of options for modal, including 'title', 'width', 'height', and 'fixedCenter'.
*
* @return
*   Void
*/
function ManagerModal(id, opts) {
    this.modal = $(document.getElementById(id));
    this.modalPageCover = $('#modalPageCover');

    if (opts == null) {
        this.opts = {};
    }
    else {
        this.opts = opts;
    }

    // title
    if (this.opts["title"]) {
        var title = $(this.modal.find("span#modalTitle")[0]);
        title.html(this.opts["title"]);
    }

    // size
    if (this.opts["width"]) {
        this.modal.width(this.opts["width"]);
    }
    else {
        this.modal.width(835);
    }

    if (this.opts["height"]) {
        this.modal.height(this.opts["height"]);
    }
    else {
        this.modal.height(565);
    }

    var header = $(this.modal.find('div.header')[0]);
    header.height(this.HEADER_HEIGHT);

    var body = $(this.modal.find('div.body')[0]);
    body.height(this.modal.height() - this.HEADER_HEIGHT - 2);

    var headerPadding = $(header.find('div.headerPadding')[0]);
    // 1 extra pixel for the border
    headerPadding.height(this.HEADER_HEIGHT - 20 - 1);

    // position
    if (this.opts["fixedcenter"] == true) {
        var viewportWidth = $(window).width();
        var viewportHeight = $(window).height();
        var scrollHeight = $(window).scrollTop();

        var height = this.modal.height();
        var width = this.modal.width();

        this.modal.css({ "left": (viewportWidth - width) / 2,
            "top": scrollHeight + ((viewportHeight - height) / 2)
        });
    }

    var messageOk = $("#modalMessageOk");
    messageOk.click(this.hideMessage);
}


/**
* Shows the ManagerModal window.
*
* @return
*   Void
*/
ManagerModal.prototype.show = function () {
    this.modal.find("div.modalContent").hide();

    var title = $(this.modal.find("span#modalTitle")[0]);
    title.html(this.opts["title"]);

    var close = $(".closeWrapper");
    var closeText = $(close.children("span"));
    closeText.html(getString("SaveAndClose"));

    this.modalPageCover.show();
    this.modal.show();
};
/**
* Hides the ManagerModal window.
*
* @return
*   Void
*/
ManagerModal.prototype.hide = function () {
    this.modalPageCover.hide();
    this.modal.hide();
};
/**
* Hides the ManagerModal window.
*
* Unlike hide, this fuction is fired from an event.
*
* @param e
*   Event object that fired the function.
*
* @return
*   Void
*/
ManagerModal.prototype.close = function (e) {
    var self = e.data;
    self.hideMessage();
    self.hide();

    var $close = $(".closeWrapper");
    $close.unbind('click');
};
/**
* Shows a message over the ManagerModal window
*
* @param message
*   Message string to be displayed.
*
* @param buttonLabel
*   String for the label of the button on the message.
*
* @return
*   Void
*/
ManagerModal.prototype.showMessage = function (message, buttonLabel) {
    var messageWrapper = $("span.messageText");
    messageWrapper.html(message);

    if (buttonLabel == null) {
        buttonLabel = getString("Ok");
    }
    $("#modalMessageOk").html(buttonLabel);

    $("#modalMessages").show();
};
ManagerModal.prototype.hideMessage = function () {
    $("#modalMessages").hide();
};
ManagerModal.prototype.HEADER_HEIGHT = 60;

ManageHelp.prototype = new ManagerModal;
ManageHelp.prototype.constructor = ManageHelp;
ManageHelp.prototype.parent = ManagerModal.prototype;

/**
* Shows a ManagerModal window containing help context for the user.
*
* @param id
*   HTML Id for the MangeHelp modal
*
* @param opts
*   Object of options to be passed when constructing the window.  See ManagerModal constructor.
*
* @return
*   Void
*/
function ManageHelp(id, opts) {
    ManagerModal.call(this, id, opts);

    this.bodyContent = $(this.modal.find("div.bodyContent")[0]);
    this.helpContent = $(this.modal.find("div.helpContent")[0]);

    this.setHelpCategories();

    var left = $(this.helpContent.find("span#quickstartLeft"));
    left.bind("click", { self: this, direction: -1 }, this.quickstartPageChange).hide();

    var right = $(this.helpContent.find("span#quickstartRight"));
    right.bind("click", { self: this, direction: 1 }, this.quickstartPageChange);

    $(this.helpContent.find("span#userguideLeft")).bind("click", { self: this, direction: -1 }, this.userguidePageChange).hide();
    $(this.helpContent.find("span#userguideRight")).bind("click", { self: this, direction: 1 }, this.userguidePageChange);

    var close = $(".closeWrapper");
    close.bind("click", this, this.close);
}

/**
* Shows a list of help options for the user to select from.
*
* @return
*   Void
*/
//ManageHelp.prototype.setHelpCategories = function () {
//    var leftBar = $(this.modal.find("div.leftBar")[0]);
//    leftBar.empty();

//    var categories = [[getString("Support"), "support"]];

//    var helpEntry;
//    for (var i = 0; i < categories.length; i++) {
//        var k = categories[i];
//        helpEntry = this.buildHelpCategory(k[0], k[1]);
//        $(helpEntry).addClass('listEntrySelected');  //remove if adding more tabs ^
//        leftBar.append(helpEntry);
//    }

//};
/**
* Shows a section of the user help documentation.
*
* @param index
*   Index of the help section to be displayed.
*
* @param view
*   This parameter has been deprecated.
* @return
*   Void
*/
ManageHelp.prototype.setViewAndShow = function (index, view) {
    this.show();
    var leftBar = $(this.modal.find("div.leftBar")[0]);
    var entry = $(leftBar.children()[index]);
    entry.click();
};
/**
* Builds a link to a help section.
*
* @param name
*   Name for the link.
*
* @param view
*   This parameter has been deprecated.
*
* @return
*   HTML string for the created link
*/
ManageHelp.prototype.buildHelpCategory = function (name, view) {
    var html = $("<div class='listEntry'><span class='listEntryText'>" + name + "</span></div>");
    html.bind("click", { self: this, view: view }, this.showHelpCategory);
    return html;
};
/**
* Shows a help category after being selected from the menu
*
* @param e
*   Event object that fired the function
*
* @return
*   Void
*/
ManageHelp.prototype.showHelpCategory = function (e) {
    var self = e.data.self;
    var view = e.data.view;

    self.bodyContent.hide();

    var target;
    if (e.currentTarget) {
        target = $(e.currentTarget);
        $.each($("div.listEntry"), function (i, v) { $(v).removeClass("listEntrySelected"); });
        target.addClass("listEntrySelected");
    }

    $("div.helpView").hide();
    $("#" + view).show();
};
/**
* Changes the current page in the quickstart guide.
*
* @param e
*   Event object that fired the function.
*
* @return
*   Void
*/
ManageHelp.prototype.quickstartPageChange = function (e) {
    var direction = e.data.direction;
    var self = e.data.self;

    var img = $("#quickstartImage");
    var src = img.attr("src");

    var index = src.search(/[1-8]/);

    if (index != -1) {
        var newPage = parseInt(src.charAt(index)) + direction;

        if (newPage >= 1 && newPage <= 8) {

            //hide or show the arrows based on the page we are on
            if (newPage === 1) { //page is not zero indexed
                $('span#quickstartLeft').hide();
            } else if (newPage === 8) {
                $('span#quickstartRight').hide();
            } else {
                $('span#quickstartLeft').show();
                $('span#quickstartRight').show();
            }

            src = src.replace(src.charAt(index), newPage);
            var date = new Date();
            img.attr("src", src); //+ "?v=" + date.getTime());
        }
    }
};

/*
* TODO: Combine with pervious function to create one generic function w/o having to duplicate code
*
* Changes the current page in the user guide.
*
* @param e
*   Event object that fired the function.
*
* @return
*   Void
*/
ManageHelp.prototype.userguidePageChange = function (e) {
    var direction = e.data.direction;
    var self = e.data.self;

    var img = $("#userguideImage");
    var src = img.attr("src");

    var index = src.search(/[1-8]/);

    if (index != -1) {
        var newPage = parseInt(src.charAt(index)) + direction;

        if (newPage >= 1 && newPage <= 8) {

            //hide or show the arrows based on the page we are on
            if (newPage === 1) { //page is not zero indexed
                $('span#userguideLeft').hide();
            } else if (newPage === 8) {
                $('span#userguideRight').hide();
            } else {
                $('span#userguideLeft').show();
                $('span#userguideRight').show();
            }

            src = src.replace(src.charAt(index), newPage);
            var date = new Date();
            img.attr("src", src);
        }
    }
};

/**
* Shows the ManageHelp window over the current screen
*
* @param e
*   Event object that fired the function
*
* @return
*   Void
*/
ManageHelp.prototype.show = function (e) {
    this.parent.show.call(this);

    var close = $(".closeWrapper");    
    var closeText = $(close.children("span"));
    closeText.html(getString("Close"));

    this.helpContent.show();
    this.setHelpCategories();
};
/**
* Closes the ManageHelp window.
*
* @param e
*   Event object that fired the function
*
* @return
*   Void
*/
ManageHelp.prototype.close = function (e) {
    var self = e.data;
    self.helpContent.hide();
    self.parent.close.call(self, { data: self });
};
ManageUsers.prototype = new ManagerModal;
ManageUsers.prototype.constructor = ManageUsers;
ManageUsers.prototype.parent = ManagerModal.prototype;

/**
* Creates a ManageUsers modal screen to edit users.
*
* @param id
*   HTML Id for the ManageUsers window.
*
* @param opts
*   Object of options to be passed when constructing the window.  See ManagerModal constructor. 
*
* @return
*   New instance of ManageUsers
*/
function ManageUsers(id, opts) {
    ManagerModal.call(this, id, opts);

    this.bodyContent = $(this.modal.find("div.bodyContent")[0]);
    this.editUserContent = $(this.modal.find("div.editUserContent")[0]);

    var close = $(".closeWrapper");
    close.bind("click", this, this.close);
}

/**
* Builds an HTML list of users in the ManageUsers window.
*
* @param users
*   List of users on the account.
*
* @return
*   Void
*/
ManageUsers.prototype.setUsers = function(users) {
    var leftBar = $(this.modal.find("div.leftBar")[0]);
    leftBar.empty();

    var userListEntry;
    for (var i = 0; i < users.length; i++) {
        userListEntry = this.buildUserListEntry(users[0]);
        leftBar.append(userListEntry);
    }

    var firstUser = $(leftBar).children(":first");
    firstUser.click();
};

/**
* Enters a screen to edit a selected user
*
* @param e
*   Event object that fired the function
*
* @return
*   Void
*/
ManageUsers.prototype.setUser = function (e) {
    var self = e.data.self;
    var user = e.data.user;

    if (e.currentTarget) {
        var target = $(e.currentTarget);

        $.each($("div.listEntry"), function (i, v) { $(v).removeClass("listEntrySelected"); });
        target.addClass("listEntrySelected");
    }

    var input;
    for (var k in user) {
        if (!k) {
            continue;
        }
        var str = "";
        if (k == USER_MAILING_LIST_OPT_IN) {
            $("#" + k).prop("checked", user[k]);
            continue;
        } if (k == "TimeZoneId") {
            var timezoneId = user["TimeZoneId"],
                  timezoneVal = user["UseDaylightSavings"]
                      ? timezoneId + "#DST"
                      : timezoneId;

            $("#TimeZone").val(timezoneVal);
            continue;
        } else {
            str = user[k];
        }
        input = $("#" + k);
        input.val(str);
    }
    regionDisable(user['CountryCode']);
};

/**
* Handles the change event for the CountryCode
**/
$(function () {
    $("#CountryCode").on("change", function () {
        regionDisable($("#CountryCode").attr('value'));
    });
});

/**
* Takes a country and determines if the state input should be disabled.
*
* @param country
*   Country in which the user has selected 
*/
function regionDisable(country) {
    var region = $.find('#RegionCode');
    if (country != "USA" && country != "CAN" && country != "MEX" && country != "PRI") {
        $(region).attr('disabled', true);
    } else {
        $(region).attr('disabled', false);
    }
}

/**
* Builds an HTML list entry for a single user.
*
* @param user
*   User for which to build a list entry.
*
* @return
*   HTML string for the new list entry.
*/
ManageUsers.prototype.buildUserListEntry = function(user) {
    // for now only one user so a defualt value
    var userId = 1;
    var userName = user[USER_FIRST_NAME] + " " + user[USER_LAST_NAME];
    var html = $("<div id='user_" + userId + "' class='listEntry'><span class='listEntryText'>" + userName + "</span></div>");
    html.bind("click", { self: this, user: user }, this.setUser);
    return html;
};

/**
* Saves the edited user profile info to the server.
*
* @return
*   Void
*/
ManageUsers.prototype.saveUserInfo = function () {
    var newUserData = {};

    var input, inputId;
    var inputs = $("div.editUserContent").find(".userInfo");

    newUserData["FirstName"] = $("#FirstNameInput").val();
    newUserData["LastName"] = $("#LastNameInput").val();

    if ($("#StreetPostalCode").val() == "" || $("#FirstName").val() == "" || $("#LastName").val() == "") {
        new Alert({
            title: getString("EnumerateFailedTitle"),
            message: getString("AccountMissingInfoMessage"),
            buttons: [{ text: getString("Ok"), handler: function (e) { e.data.alert.hide(); } }]
        }).show();
    } else {
        for (var i = 1; i < inputs.length; i++) {
            input = $(inputs[i]);
            inputId = input.attr('id');
            if (inputId == USER_MAILING_LIST_OPT_IN) {
                newUserData[inputId] = input.prop("checked");
            } else {
                newUserData[inputId] = input.val();
            }
        }

        PageMethods.saveUserProfile(newUserData, this.saveUserCallback, handleGenericError, { self: this, userData: newUserData });

    }
};

/**
* Fires after the user profile is saved to the server, and closes the editing page on success.
*
* @param r
*   JSON response from the server to the saveUserInfo request
*
* @param obj
*   Object passed through saveUserInfo server request containing the ManageUsers instance.
*
* @return
*   Void
*/

ManageUsers.prototype.saveUserCallback = function (r, obj) {
    var self = obj.self;
    if (r.ReturnCode == 0) {
        var userData = obj.userData;
        users = [userData];

        // this is just while we only have one username
        $("#account_username").html(userData[USER_FIRST_NAME] + " " + userData[USER_LAST_NAME]);
        self.parent.close.call(self, { data: self });
    } else {
        self.showMessage(r.ErrorMessage);
    }
};
/**
* Closes the ManageUsers window
*
* @param e
*   Event object that fired the function.
*
* @return
*   Void
*/
ManageUsers.prototype.close = function (e) {
    var self = e.data;
    self.saveUserInfo();
};
/**
* Shows the ManageUsers window
*
* @return
*   Void
*/
ManageUsers.prototype.show = function () {
    this.parent.show.call(this);
    this.modal.find("div.editUserContent").show();
};
ManageDevices.prototype = new ManagerModal;
ManageDevices.prototype.constructor = ManageDevices;
ManageDevices.prototype.parent = ManagerModal.prototype;

/**
* Contructs a ManagerModal windows in which to add, remove, and edit the devices on an account.
*
* @param id
*   HTML Id for the ManageDevices modal window.
*
* @param opts
*   Object of options for modal, including 'title', 'width', 'height', and 'fixedCenter'.
*
* @return
*   Void
*/
function ManageDevices(id, opts) {
    ManagerModal.call(this, id, opts);

    this.deviceWatchList = [];
    this.learning = false;

    this.bodyContent = $(this.modal.find("div.bodyContent")[0]);
    this.addNewDeviceContent = $(this.modal.find("div.addNewDeviceContent")[0]);
    this.addNewGatewayContent = $(this.modal.find("div.addNewGatewayContent")[0]);

    this.devicePicker = $(this.modal.find("div#deviceTypeLearn"));
    this.genericLearn = $(this.modal.find("div#genericLearnProcess"));
    this.gateLearn = $(this.modal.find("div#gateLearnProcess"));

    this.genericLearnInstructions = $(this.modal.find("#genericLearnInstructions"));
    this.garageLearnInstructions = $(this.modal.find("#garageDoorLearnInstructions"));
    this.cdoLearnInstructions = $(this.modal.find("#commercialDoorLearnInstructions"));

    this.learnImage = $(this.modal.find("img.learnImage"));
    this.learnImage1 = $(this.modal.find("img#learnimage1"));
    this.learnImage3 = $(this.modal.find("img#learnimage3"));
    this.learnImage.unbind("mouseover");
    this.learnImage.unbind("mouseout");
    this.learnImage.mouseover(function (e) {
        $(e.currentTarget).animate({ width: "200" }, function () { });
    });
    this.learnImage.mouseout(function (e) {
        $(e.currentTarget).animate({ width:"115" }, function () { });
    });

    this.learnProcessWrapper = null;

    var close = $(".closeWrapper");
    close.bind("click", this, this.close);
}

/**
* Closes the ManageDevices modal window and saves any newly entered data.
*
* @param e
*   Event object that fired the function.
*
* @return
*   Void
*/
ManageDevices.prototype.close = function (e) {
    var self = e.data;

    if (self.learning) {
        deviceManager.turnOffLearnMode(self.gatewayId, function () { });
    }

    self.deviceUpdateCount = 0;
    if (self.deviceWatchList.length > 0) {
        for (var i = 0; i < self.deviceWatchList.length; i++) {
            var name = $($("#device_" + self.deviceWatchList[i]).find("input")[0]);
            if (name && name.val() != "") {
                deviceManager.setDeviceAttribute(self.deviceWatchList[i], Device.NAME, name.val(), self.saveNamesCallback, self);
            }
            else {
                // CANNOT NAME DEVICE EMPTY STRING
                self.showMessage(getString("EmptyDeviceNameWarning"));
            }
        }
    }
    else {
        self.parent.close.call(self, { data: self });
    }
};
/**
* Callback from the server request to update device names.
*
* @param r
*   JSON server response to saving the names of the devices.
*
* @param self
*   ManageDevices instance passed through setDeviceAttribute server call
*
* @return
*   Void
*/
ManageDevices.prototype.saveNamesCallback = function (r, self) {
    self.deviceUpdateCount++;
    if (self.deviceUpdateCount == self.deviceWatchList.length) {
        self.parent.close.call(self, { data: self });
    }
};
/**
* Constructs a menu of selectable gateways. 
*
* @param gatewayList
*   List of gateway objects that registered with the account.
*
* @return
*   Void
*/
ManageDevices.prototype.setGateways = function (gatewayList) {
    var leftBar = this.modal.find("div.leftBar").eq(0);
    leftBar.empty();

    var gatewayListEntry;
    for (var i = 0; i < gatewayList.length; i++) {
        gatewayListEntry = this.buildGatewayListEntry(gatewayList[i]);
        leftBar.append(gatewayListEntry);
    }

    var a = this.makeAddNewElement(getString("AddNewPlace"));
    a.bind('click', { self: this }, this.addGateway);
    leftBar.append(a);
    leftBar.append(getString("MyQGarageNote"));
    leftBar.find("#modalNote").hide();
    var firstGateway = $(leftBar).children(":first");
    firstGateway.click();
};
/**
* Constructs an HTML button that can be used to add new devices through the ManageDevices modal.
*
* @param string
*   String label for the button.
*
* @return
*   HTML string for the new button.
*/
ManageDevices.prototype.makeAddNewElement = function (string) {
    var html = $("<div class='addNew'><span class='brandText'>+ </span>" + string + "</div>");
    return html;
};
/**
* Removes a device from a list in the ManageDevices modal window. 
*
* @param id
*   Id of device which is to be removed.
*
* @return
*   Void
*/
ManageDevices.prototype.deviceRemoved = function (id) {
    $("#device_" + id).remove();
    var deviceList = $("#manageDeviceList");
    var devices = deviceList.children(".deviceEntry");

    if (devices.length == 0) {
        deviceList.append(this.buildEmptyDeviceEntry());
    }
    else {
        $(devices[devices.length - 1]).addClass("lastManageDeviceEntry");
    }
};
/**
* Builds and displays a list of devices for a given gateway.
*
* @param e
*   Event object that fired the funciton.
*
* @return
*   Void
*/
ManageDevices.prototype.setDevices = function (e) {
    var self = e.data.self;
    var gateway = e.data.gateway;

    var devices = gateway.getDevices();
    var list = [];
    for (var k in devices) {
        list.push(devices[k]);
    }
    devices = list.sort(deviceDateSort);

    if (e.currentTarget) {
        var target = $(e.currentTarget);

        $.each($("div.gatewayEntry"), function (i, v) { $(v).removeClass("gatewayEntrySelected"); });
        target.addClass("gatewayEntrySelected");
    }

    var body = $(self.modal.find("div.bodyContent")[0]);
    body.empty();

    var g = self.buildGatewayEntry(gateway);
    body.append(g);

    var deviceWrapper = $("<div id='manageDeviceList'><span class='brandText deviceListHeader'>" + gateway.name() + " " + getString("Devices") + "</span></div>");
    body.append(deviceWrapper);

    if (devices.length > 0) {
        for (var i = 0; i < devices.length; i++) {
            if (typeof devices[i] === "object") {
                var d = self.buildDeviceEntry(devices[i], gateway);
                var c = self.buildDeleteConfirmation(devices[i], gateway);
                d.append(c);
                deviceWrapper.append(d);

                d.addClass("lastManageDeviceEntry");
            }
        }
    }
    else {
        deviceWrapper.append(self.buildEmptyDeviceEntry());
    }
    
    var a = self.makeAddNewElement(getString("AddNewDevice"));
    a.bind('click', null, function () {
        //self.addDevice(gateway);
        self.currentGateway = gateway;
        self.showDevicePicker();
    });
    
    body.append(a);
    body.append(getString("MyQGarageNote"));
    self.addNewDeviceContent.hide();
    self.addNewGatewayContent.hide();

    self.modal.find("div.leftBar #modalNote").hide();
    
    body.show();
};

// Device Management Methods. These are abstracted out from the html event handlers because its just feels wrong
//to have the main function to manage your devices being an event handler. They should be more generic.
ManageDevices.prototype.deleteDevice = function (deviceId) {
};
/**
* Handles keypresses within the gateway serial number entry boxes.  Automatically navigates among boxes when characters are added and deleted.
*
* @param e
*   Event object that fired the function.
*
* @return
*   Void
*/
ManageDevices.prototype.gatewaySerialKeypress = function (e) {
    var input = $(e.currentTarget);
    var maxTextLength = input.attr("maxlength");

    var previousInput = $(input.prev(".gatewaySerialPart"));
    var nextInput = $(input.next(".gatewaySerialPart"));
    
    // looks like delete is 8
    if (e.keyCode != 8 && input.val().length == maxTextLength && nextInput != null) {
        nextInput.focus();
    }
    else if (e.keyCode == 8 && input.val().length == 0 && previousInput != null) {
        previousInput.focus();
    }

    return true;
};
/**
* Attempts to add a gateway with the given serial number to the account.
*
* @param e
*   Event object that fired the function.
*
* @return
*   Void
*/
ManageDevices.prototype.addGateway = function (e) {
    var self = e.data.self;

    self.bodyContent.hide();
    self.addNewDeviceContent.hide();
    self.addNewGatewayContent.show();
    self.modal.find("div.leftBar #modalNote").hide();

    self.addGatewayLoader = $('#addGatewayLoader');
    self.addGatewayLoader.css('visibility', 'hidden');

    $("#addGatewayStepTwo").addClass("inactiveStep");
    $("#addGatewayStepOne").removeClass("inactiveStep");
    $("input#newGatewayName").val("");

    var serialParts = $("input.gatewaySerialPart");
    serialParts.val("");
    serialParts.keyup(self.gatewaySerialKeypress);

    $('#addGatewayButton').removeAttr('disabled');
    self.addGatewayButton = $('#addGatewayButton');
    self.addGatewayButton.disabled = false;
    self.addGatewayButton.unbind('click');
    self.addGatewayButton.click(function () {
        self.registerGateway();
    });
};
/**
* Determines whether the given serial number is valid for a new gateway and passes that serial number to the device manager to add the gateway to the account.
*
* @return
*   Void
*/
ManageDevices.prototype.verifySerial = function () {
    var self = this;
    if (deviceManager) {
        var obj = { self: self };
        var serialNumber = $("#gatewaySerialNumber1").val() + $("#gatewaySerialNumber2").val() + $("#gatewaySerialNumber3").val();
        var result = deviceManager.verifyGatewaySerial(serialNumber, self.gatewayAddedCallback, obj);
        if (result == DeviceManager.GATEWAY_SERIAL_REGISTERING) {
            self.addGatewayLoader.css('visibility', 'visible');
        }
        else if (result == DeviceManager.GATEWAY_SERIAL_INVALID_BRAND) {
            self.showMessage(getString("WrongGatewayPrefixMessage"));
        }
        else if (result == DeviceManager.GATEWAY_SERIAL_INVALID_LENGTH) {
            self.showMessage(getString("RegisterGatewayInvalidLengthMessage"));
        }
    }
};
ManageDevices.prototype.registerGateway = function () {
    var self = this;
    if (deviceManager) {
        $('#addGatewayButton').children('i').show();
        var serialNumber = $("#gatewaySerialNumber1").val() + $("#gatewaySerialNumber2").val() + $("#gatewaySerialNumber3").val();
        //self.addGatewayLoader.show().css('visibility', 'visible');
        deviceManager.registerGateway(serialNumber, self);
    }
};
/**
* Fired after a gateway is successfully added to the account.  Moves windows to the next step in the add gateway process.
*
* @param gatewayId
*   Id of the gateway which was just added to the account.
*
* @param obj
*   Object used to pass an instance of ManageDevices through the server response.
*
* @return
*   Void
*/
ManageDevices.prototype.gatewayAddedCallback = function (gatewayId, obj) {
    var self = obj.self;
    if (gatewayId != -1) {
        //getDeviceList();
        //self.addGatewayLoader.css('visibility', 'hidden');
        //$("#addGatewayStepTwo").removeClass("inactiveStep");
        //$("#addGatewayStepOne").addClass("inactiveStep");
        $("input#newGatewayName").val(deviceManager.getDefaultNameForDeviceType(TYPE_GATEWAY));
        
        //$("#saveGatewayName").unbind('click');
        //$("#saveGatewayName").click(function() {
        //    var gatewayName = $("input#newGatewayName").val();
        //    var newGatewayId = gatewayId;
        //    if (gatewayName != "") {
        //        deviceManager.setDeviceAttribute(newGatewayId, Device.NAME, gatewayName, self.nameGatewayCallback, { self: self, gatewayName: gatewayName, newGatewayId: newGatewayId });
        //    } else {
        //        self.showMessage(getString("EmptyDeviceNameWarning"));
        //    }
        //});
    }
    else {
        $('#addGatewayButton').children('i').hide();
        //self.addGatewayLoader.css('visibility', 'hidden');
        self.showMessage(getString("GatewayAddGenericErrorMessage"));
        $(".gatewaySerialPart").val("");
    }
};
/**
* Fired after a gateway is successfully renamed on the account.
*
* @param r
*   JSON response from the server for setting the device name attribute.
*
* @param obj
*   Object used to pass an instance of ManageDevices through the server response.
*
* @return
*   Void
*/
ManageDevices.prototype.nameGatewayCallback = function (r, obj) {
    var self = obj.self;
    gatewayName = obj.gatewayName;
    gatewayId = obj.newGatewayId;
    gateways[gatewayId].deviceName[0] = gatewayName;
    self.addNewGatewayContent.hide();
    self.setGateways(getSortedGatewayList());

    myqr.hideAllCards();
    $('.card_managePlaces').fadeIn("fast");

    $("#gatewaySerialNumber1").val("");
    $("#gatewaySerialNumber2").val("");
    $("#gatewaySerialNumber3").val("");

    getDeviceList();
    
    var newGateway = $("#gateway_" + gatewayId);
    if (newGateway) {
        newGateway.click();
    }

    //getDeviceList();
};
ManageDevices.prototype.setDeviceLearn = function (deviceType) {
    //deviceType
    // 0 == gdo
    // 1 == light
    // 2 == gate
    // 3 == CDO

    var deviceName = "";
    var learnImagePath1 = "/Assets/";
    var learnImagePath3 = "/Assets/";
    this.devicePicker.hide();

    // generic process
    if (deviceType == 0 || deviceType == 1 || deviceType == 3) {
        if (deviceType == 0) {
            deviceName = getString("DefaultGDO");
            learnImagePath1 = getString("GDOLearnImage1URL");
            learnImagePath3 = getString("GDOLearnImage3URL");
            this.genericLearnInstructions.hide();
            this.cdoLearnInstructions.hide();
            this.garageLearnInstructions.show();
            this.modal.find("div.leftBar #modalNote").show();
        }
        else if (deviceType == 1) {
            deviceName = getString("DefaultLight");
            learnImagePath1 += "LearnImage_Lights.png";
            learnImagePath3 += "LearnImage_Lights.png";
            this.genericLearnInstructions.show();
            this.garageLearnInstructions.hide();
            this.cdoLearnInstructions.hide();
        }
        else {
            deviceName = getString("DefaultCDO");
            learnImagePath1 = getString("CDOLearnImage");
            learnImagePath3 = getString("CDOLearnImage");
            this.genericLearnInstructions.hide();
            this.garageLearnInstructions.hide();
            this.cdoLearnInstructions.show();
            this.modal.find("div.leftBar #modalNote").show();
        }
        this.gateLearn.hide();
        this.genericLearn.show();
        this.learnProcessWrapper = this.genericLearn;
    }
    // gate process
    else if (deviceType == 2) {
        deviceName = getString("GateOperator");
        this.gateLearn.show();
        this.genericLearn.hide();
        this.cdoLearnInstructions.hide();
        this.learnProcessWrapper = this.gateLearn;
        learnImagePath1 += "LearnImage_Gates.png";
        learnImagePath3 += "LearnImage_Gates.png";
    }

    this.modal.find("#learnProcessModalNote").hide();

    $("span.addDeviceName").each(function (i) {
        $(this).html(deviceName);
    });

    this.learnImage1.attr("src", learnImagePath1);
    this.learnImage3.attr("src", learnImagePath3);
    this.addDevice(this.currentGateway);
};
/**
* Shows device options for what to learn 
*
* @return
*   Void
*/
ManageDevices.prototype.showDevicePicker = function () {
    this.bodyContent.hide();
    this.genericLearn.hide();
    this.gateLearn.hide();
    this.addNewGatewayContent.hide();
    this.devicePicker.show();
    this.modal.find("#learnProcessModalNote").show();
    this.addNewDeviceContent.show();
    $(".deviceName").val("");
};
/**
* Shows a dialog for adding a new device to a given gateway.
*
* @param gateway
*   Gateway object to which the device should be added.
*
* @return
*   Void
*/
ManageDevices.prototype.addDevice = function (gateway) {
    var self = this;
    self.gateway = gateway;
    self.gatewayId = gateway.deviceId;
    self.deviceList = [];

    $("div.addStep").addClass("inactiveStep");
    $("div.addStep1").removeClass("inactiveStep");
    $("#deviceName").val("");

    self.addDeviceLoader = self.learnProcessWrapper.find(".addDeviceLoader");
    self.addDeviceLoader.css("visibility", "hidden");

    self.waitForLearnLoader = self.learnProcessWrapper.find(".stepTwoAddDeviceLoader");
    self.waitForLearnLoader.css("visibility", "hidden");

    var nextStepButton = self.learnProcessWrapper.find(".nextStepButton");
    var nextStep = self.learnProcessWrapper.find(".addStep2"); 
    // so we only have one event
    nextStepButton.unbind('click');
    nextStepButton.click(function () {
        nextStep.removeClass("inactiveStep");
    });

    var addDeviceButton = self.learnProcessWrapper.find(".addDeviceButton");
    addDeviceButton.unbind("click");
    addDeviceButton.click(function () {
        self.addDeviceLoader.css('visibility', 'visible');
        deviceManager.getDevices(self.gatewayId, self.getOrigDevicesCallback, self);
    });

	if ( self.learnProcessWrapper == self.gateLearn )
	{
		var next = self.learnProcessWrapper.find(".gateStepThreeNextButton");
		next.unbind("click");
		next.click(function() {
			self.learnProcessWrapper.find(".addStep4").removeClass("inactiveStep");
			self.waitForLearnLoader.css("visibility", "visible");
		});
	}

    var saveNameButton = self.learnProcessWrapper.find(".saveDeviceName");
    saveNameButton.unbind('click');
    saveNameButton.click(function () {
        self.saveNameButtonClick();
    });
};
/**
* Builds a list of devices that are synced to a gateway and puts the gateway into learn mode.
*
* @param r
*   JSON response from the server to getDevices request.
*
* @param self
*   Instance of ManageDevices passed through server request.
*
* @return
*   Void
*/
ManageDevices.prototype.getOrigDevicesCallback = function(r, self) {
    if (r != null) {
        self.deviceList = r;
        deviceManager.enableLearnMode(self.gatewayId, self.gatewayLearnModeCallback, self);
    } else {

    }
};

/**
* Fired after a gateway is put into learn mode.
*
* @param r
*   JSON response from the server to enableLearnMode request.
*
* @param gatewayId
*   Id of gateway which was put into learn mode.
*
* @param self
*   Instance of ManageDevices passed through server request.
*
* @return
*   Void
*/
ManageDevices.prototype.gatewayLearnModeCallback = function(result, gatewayId, self) {
    self.addDeviceLoader.css('visibility', 'hidden');
    if (result.ReturnCode === 0) {
        self.learning = true;
        self.learnProcessWrapper.find(".addStep3").removeClass("inactiveStep");

        if (self.learnProcessWrapper == self.genericLearn) {
            self.waitForLearnLoader.css('visibility', 'visible');
        }
        deviceManager.waitForGatewayOff(gatewayId, 180, self.watchForGatewayOffCallback, self);
    } else {
        if (canPassErrorToUser(result.ReturnCode)) {
            self.showMessage(result.ErrorMessage);
        } else {
            self.showMessage(getString("GatewayLearnModeFailedMessage"));
        }
    }
};

/**
* Fired after a gateway has learned a new device or has timed out trying to learn a new device.
*
* @param r
*   JSON response from the server to waitForGatewayOff request.
*
* @param gatewayId
*   Id of gateway which was put into learn mode.
*
* @param self
*   Instance of ManageDevices passed through server request.
*
* @return
*   Void
*/
ManageDevices.prototype.watchForGatewayOffCallback = function(r, gatewayId, self) {
    self.waitForLearnLoader.css('visibility', 'hidden');
    if (r == 0 && self.learning) {
        deviceManager.getDevices(gatewayId, self.getDevicesCallback, self);
    } else {
        self.showMessage(getString("DeviceAddFailedMessage"));
    }
};

/**
* Fired after a gateway come out of learn mode. Adds elements for a new device if a new device was learned.
*
* @param r
*   JSON response from the server to getDevices request.
*
* @param self
*   Instance of ManageDevices passed through server request.
*
* @return
*   Void
*/
ManageDevices.prototype.getDevicesCallback = function (r, self) {
    self.waitForLearnLoader.css('visibility', 'hidden');
    var oldDeviceIds = getDeviceKeys(self.deviceList);
    var newDeviceIds = getDeviceKeys(r);
    var result = diffArray(newDeviceIds, oldDeviceIds);
    if (result.length > 0) {

        selector = "";
        if (self.learnProcessWrapper == self.gateLearn) {
            selector = ".addStep5";
        }
        else {
            selector = ".addStep4";
        }

        var lastStep = self.learnProcessWrapper.find(selector);
        lastStep.removeClass("inactiveStep");

        self.newDeviceId = result[0];
        getDeviceList(function (deviceListResult) {
            for (var i = 0; i < r.length; i++) {
                if (r[i]["DeviceId"] == self.newDeviceId) {
                    self.deviceType = r[i]["TypeId"];
                    $(".deviceName").val(deviceManager.getDefaultNameForDeviceType(self.deviceType));

                    var device = devices[r[i]["DeviceId"]];
                    if (device != null) {
                        device.setOnline(true);
                    }
                }
            }
        });
    }
    else {
        // device was not added
        self.showMessage(getString("DeviceAddFailedMessage"));
    }
};
/**
* Sends a server request to change of the name of the current device.
*
* @return
*   Void
*/
ManageDevices.prototype.saveNameButtonClick = function () {
    var self = this;
    var devName = self.learnProcessWrapper.find("input.deviceName").val();
    if (devName != "") {
        deviceManager.setDeviceAttribute(self.newDeviceId, Device.NAME, devName, self.nameDeviceCallback, { self: self, devName: devName, devId: self.newDeviceId });
    } else {
        self.showMessage(getString("EmptyDeviceNameWarning"));
    }
};
/**
* Fired after an attempt to rename a device.  Updates visual elements based on the new device name and closes the add device dialog.
*
* @param r
*   JSON response from the server to setDeviceAttribute name request.
*
* @param opts
*   Object containing the ManageDevices instance, new device name, and device id number.
*
* @return
*   Void
*/
ManageDevices.prototype.nameDeviceCallback = function (r, opts) {
    var self = opts["self"];
    var devName = opts["devName"];
    var devId = opts["devId"];
    devices[devId].deviceName[0] = devName;
    self.addNewDeviceContent.hide();
    var e = { data: { self: self, gateway: self.gateway} };
    getDeviceList();
    self.setDevices(e);
};
/**
* Sends a request to the server to delete a device or gateway after a user has confirmed that he/she wants it removed.
*
* @param device
*   Device object to be removed. To remove a gateway supply the gateway id as the device parameter and leave the gateway parameter null.
*
* @param gateway
*   Gateway from which to remove the device.  If this value is null and the device parameter is a gateway, remove that gateway.
*
* @return
*   Void
*/
ManageDevices.prototype.deleteDeviceConfirmed = function (device, gateway) {
    var callbackObj;
    if (gateway != null) {
        callbackObj = { self: this, device: device, gateway: gateway };
        PageMethods.removeDeviceFromUser(device.deviceId, this.deleteDeviceCallback, handleGenericError, callbackObj);
    } else {
        callbackObj = { self: this, gateway: device };
        PageMethods.removeDeviceFromUser(device.deviceId, this.deleteGatewayCallback, handleGenericError, callbackObj);
    }

    $("#delete_" + device.deviceId).html(getString("DeleteProgressTitle") + "<img class='deleteGatewayLoader' src='" + assetDirectory + "loader.gif'/>");
};

/**
* Fired after a request is sent to the server to remove a gateway.
*
* @param r
*   Server response to remove gateway request.
*
* @param obj
*   Object containing the ManageDevices instance, and removed gateway Id
*
* @return
*   Void
*/
ManageDevices.prototype.deleteGatewayCallback = function (r, obj) {
    var gatewayId = obj.gateway.deviceId;
    var self = obj.self;
    if (r.ReturnCode == 0) {
        self.bodyContent.empty();
        delete gateways[gatewayId];
        getDeviceList();
        var gatewayListObject = $("div#gateway_" + gatewayId);
        gatewayListObject.remove();
        var leftBar = $(self.modal.find("div.leftBar")[0]);
        var firstGateway = $(leftBar).children(":first");
        firstGateway.click();
        try {
            MYQ.loadRuleData();
        } catch (e) {
            try {
                if (console && console.log) {
                    console.log('Error initializing rule data');
                }
            } catch(ex) {
            }
        }
    }
    else {

        $("#delete_" + gatewayId).remove();
        var entry = self.buildDeleteGatewayConfirmation(obj.gateway);
        var box = $("#device_" + gatewayId);
        if (box.hasClass("manageGatewayEntryBox")) {
            box.append(entry);
        }
        self.showMessage(getString("GatewayRemoveFailed"));
    }
    $('#addGatewayButton').removeAttr('disabled');
};
/**
* Fired after a request is sent to the server to remove a device. If successful, device is removed from interface.
*
* @param r
*   Server response to remove device request.
*
* @param obj
*   Object containing the ManageDevices instance, and removed device Id
*
* @return
*   Void
*/
ManageDevices.prototype.deleteDeviceCallback = function (r, obj) {
    if (r.ReturnCode == 0) {
        var deviceId = obj.device.deviceId;
        removeDevice(deviceId);
        try {
            MYQ.initializeRuleData();
        } catch (e) {
            try {
                if (console && console.log) {
                    console.log('Error initializing rule data');
                }
            } catch(ex) {
            }
        }
    }
    else {
        var deviceId = obj.device.deviceId;
        var self = obj.self;
        $("#delete_" + deviceId).animate({
            height: 'toggle'
        }, 0);
        self.showMessage(getString("DeviceRemoveFailed"));
    }
};
/**
* Displays delete confirmation dialog after user selects the delete button for a device.
*
* @param e
*   Event object that fired the function.
*
* @return
*   Void
*/
ManageDevices.prototype.deleteDeviceClickedMult = 1;
ManageDevices.prototype.deleteDeviceClicked = function (e) {
    var device = e.data.device,
        $el = $("#delete_" + device.deviceId);

    $el.animate({
        height: 'toggle'
    }, {
        complete: function () {
            try {
                // Nasty solution for redraw fix for Chrome, must cause element repaint without messing up the animation toggle or losing event listeners.
                var $p = $el.parent('.deviceEntry'),
                    delta = 0.01 * ManageDevices.prototype.deleteDeviceClickedMult;

                $p.width(($p.width() + delta) + 'px');

                // Chrome ignores repeated changes using same delta, so switch between adding and subtracting the delta
                ManageDevices.prototype.deleteDeviceClickedMult = ManageDevices.prototype.deleteDeviceClickedMult === 1 ? -1 : 1;
            } catch(ex) {
            }
        }
    }, 500);

};
/**
* Adds a device to the manager watchlist.
*
* @param e
*   Event object that fired the function.
*
* @return
*   Void
*/
ManageDevices.prototype.addToWatchList = function (e) {
    var self = e.data;
    var target = $(e.currentTarget);
    var p = target.parents(".deviceEntry")[0];
    var id = p.id.split("_")[1];
    
    if (jQuery.inArray(id, self.deviceWatchList) == -1) {
        self.deviceWatchList.push(id);
    }
};
// HTML Methods
/**
* Builds a device entry that states there are no devices currently on the account.
*
* @return
*   HMTL string for new entry.
*/
ManageDevices.prototype.buildEmptyDeviceEntry = function () {
    return $("<div class='manageDeviceEntry lastManageDeviceEntry'>" + getString("NoDevicesManageMessage") + "</div>");
};
/**
* Builds a dialog that requires users to confirm deletion of a gateway.
*
* @param gateway
*   Gateway object to be deleted if confirmed.
*
* @return
*   HMTL string for deletion confirmation element.
*/
ManageDevices.prototype.buildDeleteGatewayConfirmation = function (gateway) {
    var self = this;
    var confirmDelete = $("<div id='delete_" + gateway.deviceId + "' class='confirmDelete brandText'><span>" + getString("DeleteGatewayConfirmationMessage") + "</span><br/></div>");

    var confirmButton = $("<button class='navButton' type='button'>" + getString("Yes") + "</button>");
    confirmButton.click(function () {
        self.deleteDeviceConfirmed(gateway);
    });
    var cancelButton = $("<button class='navButton' type='button'>" + getString("No") + "</button>");
    cancelButton.click(function () {
        $("#delete_" + gateway.deviceId).animate({
            height: 'toggle'
        }, 500);

    });
    confirmDelete.append(confirmButton);
    confirmDelete.append(cancelButton);

    return confirmDelete;
};
/**
* Builds a dialog that requires users to confirm deletion of a device.
*
* @param device
*   Device object to be deleted if confirmed.
*
* @param gateway
*   Gateway from which the device will be removed if confirmed.
*
* @return
*   HMTL string for deletion confirmation element.
*/
ManageDevices.prototype.buildDeleteConfirmation = function (device, gateway) {
    var self = this;
    var confirmDelete = $("<div id='delete_" + device.deviceId + "' class='confirmDelete brandText'><span>" + getString("DeleteOneDeviceConfirmationMessage") + "</span><br/></div>");

    var confirmButton = $("<button class='navButton' type='button'>" + getString("Yes") + "</button>");
    confirmButton.click(function () {
        self.deleteDeviceConfirmed(device, gateway);
    });
    var cancelButton = $("<button class='navButton' type='button'>" + getString("No") + "</button>");
    cancelButton.click(function () {
        $("#delete_" + device.deviceId).animate({
            height: 'toggle'
        }, 500);

    });

    confirmDelete.append(confirmButton);
    confirmDelete.append(cancelButton);

    return confirmDelete;
};
/**
* Builds an HTML element for a device that will appear in a list of devices affiliated with a given gateway.
*
* @param device
*   Device for which the object is built.
*
* @param gateway
*   Gateway that the device is synced to.
*
* @return
*   HMTL string for the new device list entry.
*/
ManageDevices.prototype.buildDeviceEntry = function (device, gateway) {
    var html = $("<div id='device_" + device.deviceId + "' class='manageDeviceEntry deviceEntry'><span class='equipment-icon " + device.iconFromState(true) + "' /><input type='text' class='manageDeviceName' value=\"" + device.name() + "\" /><img class='deleteDevice' src='/Assets/close_x.png' /></div>");

    var input = $(html.find("input")[0]);
    input.bind("keyup", this, this.addToWatchList);

    var deleteDevice = $(html.find("img.deleteDevice")[0]);
    deleteDevice.bind("click", { self: this, device: device, gateway: gateway }, this.deleteDeviceClicked);

    return html;
};
/**
* Builds an HTML element for a gateway that will appear in the menu for the given gateway.
*
* @param gateway
*   Gateway object for which the entry will be built.
*
* @return
*   HMTL string for the new gateway entry.
*/
ManageDevices.prototype.buildGatewayEntry = function (gateway) {
    var html = $("<div class='manageGatewayEntry brandText'><span class='deviceListHeader'>" + getString("PlaceLabel") + "</span><div id='device_" + gateway.deviceId + "' class='manageGatewayEntryBox deviceEntry'><span class='equipment-icon " + gateway.iconFromState() + "' /><input type='text' class='manageDeviceName' value=\"" + gateway.name() + "\" /><img class='deleteDevice' src='/Assets/close_x.png' /></div></div>");

    var box = $(html.find("div.manageGatewayEntryBox"));
    var gdelete = this.buildDeleteGatewayConfirmation(gateway);
    box.append(gdelete);


    var input = $(html.find("input")[0]);
    input.bind("keydown", this, this.addToWatchList);

    var deleteDevice = $(html.find("img.deleteDevice")[0]);
    deleteDevice.bind("click", { self: this, device: gateway }, this.deleteDeviceClicked);


    return html;
};
/**
* Builds an HTML element for a gateway that will appear in a list of gateways affiliated with the active user.
*
* @param gateway
*   Gateway object for which the entry will be built.
*
* @return
*   HMTL string for the new gateway list entry.
*/
ManageDevices.prototype.buildGatewayListEntry = function (gateway) {
    var gatewayId = gateway.deviceId;
    var html = $("<div id='gateway_"+gatewayId+"' class='gatewayEntry'><span class='gatewayEntryText'>" + gateway.name() + "</span></div>");
    html.bind("click", { self: this, gateway: gateway }, this.setDevices);
    return html;
};
var manageDevices = new ManageDevices();
