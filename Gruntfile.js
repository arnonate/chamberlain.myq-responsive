module.exports = function(grunt) {
  grunt.initConfig({
    pkg: grunt.file.readJSON('package.json'),
    clean: ["less/_dist/", "public/Assets/styles/", "public/Assets/scripts/scripts.*", "public/Assets/images/"],
    imagemin: {
      dynamic: {
        options: {
          optimizationLevel: 3
        },
        files: [{
          expand: true,
          cwd: 'img/',
          src: ['*.{png,jpg,gif}'],
          dest: 'public/Assets/images/'
        }]
      }
    },
    less: {
      development: {
        options: {
          paths: ['less'],
          yuicompress: false
        },
        files: {
          'less/_dist/base.css':'less/screen.less',
          'less/_dist/theme_chamberlain.css':'less/theme_chamberlain.less',
          'less/_dist/theme_liftmaster.css':'less/theme_liftmaster.less'
        }
      }
    },
    autoprefixer: {
      dist: {
        files: {
            'less/_dist/base_prefixed.css': 'less/_dist/base.css'
        }
      }
    },
    csscomb: {
        css: {
            files: {
                'public/Assets/styles/base.css': 'less/_dist/base_prefixed.css',
                'public/Assets/styles/theme_chamberlain.css': 'less/_dist/theme_chamberlain.css',
                'public/Assets/styles/theme_liftmaster.css': 'less/_dist/theme_liftmaster.css'
            }
        }
    },
    cssmin: {
      minify: {
        expand: true,
        cwd: 'public/Assets/styles/',
        src: ['*.css', '!*.min.css'],
        dest: 'public/Assets/styles/',
        ext: '.min.css'
      }
    },
    concat: {
      dist: {
        src: [
          'js/fancySelect.js',
          'js/slick.js',
          'js/main.js'
        ],
        dest: 'public/Assets/scripts/scripts.js'
      }
    },
    uglify: {
      target: {
        // options: {
        //   beautify: true
        // },
        files: {
          'public/Assets/scripts/scripts.min.js': ['public/Assets/scripts/scripts.js']
        }
      }
    },
    assemble: {
      options: {
        assets: 'public/Assets',
        partials: 'templates/partials/**/*.hbs',
        layout: 'default.hbs',
        layoutdir: 'templates/layouts',
        flatten: true
      },
      site: {
        files: {
            'ugly/': ['templates/pages/*.hbs']
        }
      }
    },
    prettify: {
        options: {
            indent: 4,
            indent_char: ' ',
            brace_style: 'expanded',
            unformatted: ['a', 'sub', 'sup', 'b', 'i', 'u']
        },
        files: {
            expand: true,
            cwd: 'ugly/',
            ext: '.html',
            src: ['*.html'],
            dest: 'public/'
        }
    },
    validation: {
      options: {
        reset: grunt.option('reset') || true,
        stoponerror: false,
        relaxerror: ["Bad value X-UA-Compatible for attribute http-equiv on element meta.","document type does not allow element \"[A-Z]+\" here"]
      },
      your_target: {
          src: ['public/*.html']
      }
    },
    watch: {
      css: {
        files: 'less/**/*.less',
        tasks: ['less','autoprefixer','csscomb'],
        options: {
            livereload: true
        }
      },
      js: {
        files: 'js/*.js',
        tasks: ['concat'],
        options: {
            livereload: true
        }
      },
      html: {
        files: ['templates/**/*.hbs'],
        tasks: ['assemble','prettify'],
        options: {
            livereload: true
        }
      },
      images: {
        files: 'img/*.{png,jpg,gif}',
        tasks: ['imagemin'],
        options: {
            livereload: true
        }
      }
    },
    connect: {
        server: {
            options: {
                port: 9001,
                base: 'public',
                keepalive: true
            }
        }
    }
  });
  grunt.loadNpmTasks('grunt-contrib-clean');
  grunt.loadNpmTasks('grunt-contrib-imagemin');
  grunt.loadNpmTasks('grunt-contrib-less');
  grunt.loadNpmTasks('grunt-contrib-cssmin');
  grunt.loadNpmTasks('grunt-contrib-concat');
  grunt.loadNpmTasks('grunt-contrib-uglify');
  grunt.loadNpmTasks('grunt-autoprefixer');
  grunt.loadNpmTasks('grunt-contrib-watch');
  grunt.loadNpmTasks('grunt-html-validation');
  grunt.loadNpmTasks('grunt-contrib-connect');
  grunt.loadNpmTasks('grunt-csscomb');
  grunt.loadNpmTasks('assemble');
  grunt.loadNpmTasks('grunt-prettify');
  grunt.registerTask('default',['clean','less','autoprefixer','csscomb','concat','cssmin','uglify','imagemin','assemble','prettify']);
};
